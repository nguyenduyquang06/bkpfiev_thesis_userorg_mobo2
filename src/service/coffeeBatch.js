import { queryChannelDB, postCC, queryCC } from "./base";
import { SecureStore } from "expo";
import _ from "lodash";

export async function getCoffeeBatchIDByCrop(cropID) {
  try {
    let resp = {};
    const uid = await SecureStore.getItemAsync("user_id");
    const body = {
      selector: {
        docType: {
          $eq: "CoffeeBatch"
        },
        cropID: {
          $eq: cropID
        },
        growerID: {
          $eq: uid
        }
      },
      fields: ["batchID"]
    };
    resp = await queryChannelDB(body);
    const res = _.map(resp.data.data.docs, o => o.batchID);
    return res;
  } catch (e) {
    throw e;
  }
}

export async function getSensorUpdateHistoryByContractID({ step, contractID }) {
  try {
    let resp = {};
    resp = await queryCC(
      "getSensorUpdateHistoryByContractID",
      step.toString(),
      contractID
    );
    return resp.data;
  } catch (e) {
    throw e;
  }
}

export async function getAllCoffeeBatch(cropID) {
  try {
    let resp = {};
    const uid = await SecureStore.getItemAsync("user_id");
    const body = {
      selector: {
        docType: {
          $eq: "CoffeeBatch"
        },
        growerID: {
          $eq: uid
        }
      }
    };
    resp = await queryChannelDB(body);
    return resp.data.data.docs;
  } catch (e) {
    throw e;
  }
}

export async function createCoffee(
  redirect,
  cropID,
  blockID,
  batchID,
  beanSize,
  weight
) {
  try {
    const { data } = await postCC(
      "createCoffee",
      cropID,
      blockID,
      batchID,
      "READY_FOR_DISTRIBUTION",
      beanSize,
      weight
    );
    return data;
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}

export async function getHistoryCoffeeBatch(redirect, params) {
  try {
    const { growerMSPID, growerID, cropID, blockID, batchID } = params;
    const { data } = await queryCC(
      "getHistoryCoffeeBatch",
      growerMSPID,
      growerID,
      cropID,
      blockID,
      batchID
    );
    return data;
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}
