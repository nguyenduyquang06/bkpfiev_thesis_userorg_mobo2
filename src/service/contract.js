import { queryDBFEC, postPrivateCC } from "./base";
import { SecureStore } from "expo";
import _ from "lodash";

export async function getContract() {
  try {
    let resp = {};
    const uid = await SecureStore.getItemAsync("user_id");
    const body = {
      selector: {
        docType: {
          $eq: "ContractFarmerExporter"
        },
        growerID: {
          $eq: uid
        }
      }
    };
    resp = await queryDBFEC(body);
    return resp.data.data.docs;
  } catch (e) {
    throw e;
  }
}

export async function acceptContract(redirect, contractID) {
  try {
    let resp = {};
    const param = {
      contractStepOne: {
        contractStepOneID: contractID
      }
    };
    resp = await postPrivateCC("acceptTradeStepOne", param, "FECGroup");
    return resp.data;
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}
