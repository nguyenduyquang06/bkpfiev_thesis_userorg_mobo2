import { queryCC, postCC, queryChannelDB } from "./base";
import { SecureStore } from "expo";
import _ from "lodash";
export async function getCropWithUID(redirect) {
  try {
    const {
      data: { message, success }
    } = await queryCC("getCropWithUID");
    if (success) {
      return {
        success,
        crops: JSON.parse(message)
      };
    }
    return {
      success
    };
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}

export async function createNewCrop(
  redirect,
  cropID,
  kindOfCoffee,
  origin,
  address
) {
  try {
    const {
      data: { success }
    } = await postCC("createNewCrop", cropID, kindOfCoffee, origin, address);
    return success;
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}

export async function getCropID() {
  try {
    let resp = {};
    const uid = await SecureStore.getItemAsync("user_id");
    const body = {
      selector: {
        docType: {
          $eq: "Crop"
        },
        growerID: {
          $eq: uid
        }
      },
      fields: ["id"]
    };
    resp = await queryChannelDB(body);
    const res = _.map(resp.data.data.docs, o => o.id);
    return res;
  } catch (e) {
    throw e;
  }
}

export async function getBlockID() {
  try {
    let resp = {};
    const uid = await SecureStore.getItemAsync("user_id");
    const body = {
      selector: {
        docType: {
          $eq: "SmartFarm"
        },
        growerID: {
          $eq: uid
        },
        ToDate: {
          $ne: ""
        }
      },
      fields: ["blockID"]
    };
    resp = await queryChannelDB(body);
    const res = _.map(resp.data.data.docs, o => o.blockID);
    return res;
  } catch (e) {
    throw e;
  }
}
