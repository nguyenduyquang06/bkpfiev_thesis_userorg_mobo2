import * as API from './base';

export const getEthBalance = async address => {
  try {    
    const {
      data: { result,message, status },
    } = await API.API_ETHERSCAN.get(`/api?module=account&action=balance&address=${address}&tag=latest`)
    return {
      message,
      result,status
    }
  } catch (err) {
    throw err
  }
}
