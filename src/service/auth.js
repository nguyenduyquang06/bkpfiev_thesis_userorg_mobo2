import {SecureStore} from 'expo';
import API, { serverGet,queryCC } from "./base"


const login = async (username, password) => {
  try {
    const body = {
      username,
      password,
      orgName: "userorg",
    }
    const {
      data: { token, success, message },
    } = await API.post("/v2/login", body)    
    if (success) {
      await SecureStore.setItemAsync('secure_token',token);
      return {
        token,
        success,
        message,
      }
    }      
    return {
      success,
      message,
    }
  } catch (err) {
    throw err
  }
}

const logout = async () => {
  try {
    // const { data } = await API.get("/logout")
    // return data
    await SecureStore.deleteItemAsync('secure_token');
    return true
  } catch (err) {
    throw err
  }
}

const getCurrentUser = async context => {
  try {
    let resp = {}
    if (context && context.res) {
      resp = await serverGet("/", context)
    }
    return resp.data.user
  } catch (e) {
    throw e
  }
}

const checkAuthorized = async context => {
  try {
    let resp = {}
    if (context && context.res) {
      resp = await serverGet("/", context)
    }
    return resp.data.user
  } catch (e) {
    const statusCode = e.response.status
    switch (statusCode) {
      case 404:
        return true
      case 401:
        return false
    }
    throw e
  }
}

const loginViaFB = async accessToken => {
  try {
    const body = {
      orgName: "userorg",
      accessToken,
    }    
    const {
      data: { token, success, message },
    } = await API.post("/v2/login", body)
    // set token

    if (success) {
      await SecureStore.setItemAsync("secure_token", token);      
    }
    return {
      token,
      success,
      message,
    }
  } catch (err) {
    throw err
  }
}

const registerAccount = async params => {
  try {    
    const body = {
      orgName: "userorg",
      username: params.email,
      attrs: params,
    }
    // console.log(body)
    const {
      data: { success, message },
    } = await API.post("/v2/register", body)
    return {
      success,
      message,
    }
  } catch (err) {
    throw err
  }
}

export { login, logout, getCurrentUser,registerAccount,loginViaFB, checkAuthorized }
