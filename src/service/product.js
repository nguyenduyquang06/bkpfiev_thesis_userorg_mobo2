import { queryDBFU, queryChannelDB, postPrivateCC } from "./base";
import { SecureStore } from "expo";
import _ from "lodash";

export async function getProductLine(productLineID, batchID) {
  try {
    let resp = {};
    const body = {
      selector: {
        docType: "ProductLine",
        id: productLineID,
        listBatch: {
          $elemMatch: {
            ID: batchID
          }
        }
      }
    };
    resp = await queryDBFU(body);
    return resp.data.data.docs[0];
  } catch (e) {
    throw e;
  }
}

export async function getCoffeeBatch(rawCoffeeBatchIDs) {
  try {
    let resp = {};
    const body = {
      selector: {
        $or:rawCoffeeBatchIDs,
        docType: "CoffeeBatch",
      }
    };
    resp = await queryChannelDB(body);
    return resp.data.data.docs;
  } catch (e) {
    throw e;
  }
}

export async function getProduct(uniqueProductID) {
  try {
    let resp = {};
    const body = {
      selector: {
        uniqueProductID,
        docType: "Product"
      }
    };
    resp = await queryDBFU(body);
    return resp.data.data.docs[0];
  } catch (e) {
    throw e;
  }
}

export async function getCrop(params) {
  try {
    let resp = {};
    const body = {
      selector: {
        $or:params,
        docType: "Crop"
      },      
    };
    resp = await queryChannelDB(body);
    return resp.data.data.docs;
  } catch (e) {
    throw e;
  }
}

export async function payProduct(params) {
  try {
    let resp = {};
    const param = {
      payment: params
    };
    resp = await postPrivateCC("payProduct", param, "FUGroup");
    return resp.data;
  } catch (e) {
    throw e;
  }
}

export async function refundProduct(params) {
  try {
    let resp = {};
    const param = {
      payment: params
    };
    resp = await postPrivateCC("refundProduct", param, "FUGroup");
    return resp.data;
  } catch (e) {
    throw e;
  }
}

export async function submitReview(params) {
  try {
    let resp = {};
    const param = {
      product: params
    };
    resp = await postPrivateCC("submitProductScore", param, "FUGroup");
    return resp.data;
  } catch (e) {
    throw e;
  }
}

export async function getReviews(productLineID) {
  try {
    let resp = {};
    const body = {
      selector: {
        docType: "Product",
        uniqueProductID: {
          productLineID,
        },
        reviewTime: {
          $ne: 0
        }
      },
      fields: ["comment", "buyTime", "reviewTime", "score", "userID"]
    };
    resp = await queryDBFU(body);
    return resp.data.data.docs;
  } catch (e) {
    throw e;
  }
}

export async function getEndUser(userID) {
  try {
    let resp = {};
    const body = {
      selector: {
        docType: "CoffeeUser",
        userID
      },
      fields: ["lastsubmitTimestamp", "productPaid", "productUsed"]
    };
    resp = await queryDBFU(body);
    return resp.data.data.docs;
  } catch (e) {
    throw e;
  }
}

export async function getReviewUsedProduct(userID) {
  try {
    let resp = {};
    const body = {
      selector: {
        docType: "Product",
        userID
      },
      fields: ["comment", "buyTime", "reviewTime", "score","uniqueProductID"]
    };
    resp = await queryDBFU(body);
    return resp.data.data.docs;
  } catch (e) {
    throw e;
  }
}

export async function getCertOfOrigin(contractStepTwoID) {
  try {
    let resp = {};
    const body = {
      selector: {
        docType: "CertOrigin",
        "_id":`\u0000CertOfOrigin\u0000${contractStepTwoID}\u0000`
      }      
    };
    resp = await queryChannelDB(body);
    return resp.data.data.docs[0];
  } catch (e) {
    throw e;
  }
}