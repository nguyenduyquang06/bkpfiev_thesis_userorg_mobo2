import { postPrivateCC, queryChannelDB,queryCC, convertToken} from "./base";

export async function getWalletBalance(userID) {
  try {
    let resp = {};
    const body = {
      selector: {
        docType: {
          $eq: "Wallet"
        },
        address: {
          $eq: userID
        }
      },
      fields: ["address", "balance"]
    };
    resp = await queryChannelDB(body);
    return resp.data.data.docs;
  } catch (e) {
    throw e;
  }
}

export async function createWallet(password) {
  try {
    let resp = {};
    const param = {
      wallet: {
        password
      }
    };
    resp = await postPrivateCC("createWallet", param);
    return resp.data;
  } catch (e) {
    throw e;
  }
}

export async function transferToken(params) {
  try {
    let resp = {};
    const param = {
      wallet: params
    };
    resp = await postPrivateCC("transferToken", param);
    return resp.data;
  } catch (e) {
    throw e;
  }
}

export async function getSystemETHAddress() {
  try {
    let resp = {}
    resp = await queryCC("getSystemETHAddress")
    return resp.data
  } catch (e) {
    throw e
  }
}
export async function getRateETHBKC() {
  try {
    let resp = {}
    resp = await queryCC("getRateETHBKC")
    return resp.data
  } catch (e) {
    throw e
  }
}
export async function convertBKCToETH(params) {
  try {
    let resp = {}
    resp = await convertToken("bkcToEth", params)
    return resp.data
  } catch (e) {
    throw e
  }
}
