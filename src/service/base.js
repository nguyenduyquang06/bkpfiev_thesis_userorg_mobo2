import axios from "axios"
import {CCVERSION,ETHERSCAN_API_END_POINT} from "react-native-dotenv"
import {SecureStore} from 'expo';
const API_END_POINT = "https://api.bkpfiev.tk"
// const API_END_POINT = "http://192.168.0.21:4000"
const API = axios.create({
  baseURL: API_END_POINT,
  timeout: 60000,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
})

// axios object for render server
const SERVER = axios.create({
  baseURL: API_END_POINT,
  timeout: 10000,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
})

/**
 * Get method for server call API
 * @param  {String} [url='']     Required. Endpoint, must start with /, ex: /auth/login
 * @param  {Object} [context={}] Required. Context is the context in getInitialProps(context)
 * @return {Promise<Response>}   Response or throw error
 */
export async function serverGet(url = "", params = {}) {
  // Get to add more cookie fields into request
  const token = await SecureStore.getItemAsync('secure_token');

  return SERVER.get(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
    data: params,
  })
}

export async function queryCC(fcnName, ...args) {  
  // Get to add more cookie fields into request
  const token = await SecureStore.getItemAsync('secure_token');
  const url = `/chaincode/${fcnName}?ccversion=${CCVERSION}&args=${JSON.stringify(
    args
  )}`
  return SERVER.get(url, {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
}

export async function postCC(fcnName, ...args) {
  const token = await SecureStore.getItemAsync('secure_token');
  const url = `/chaincode/${fcnName}`
  const params = {
    ccversion: CCVERSION,
    args,
  }
  return SERVER.post(url, params, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
}

export async function queryChannelDB(body) {
  const token = await SecureStore.getItemAsync('secure_token');
  const url = `/v2/statedb/tradechannel_yeucc09/_find`
  const body2 = {
    ...body,
    limit:10000,
  }
  return SERVER.post(url, body2, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
}

export async function queryDBFEC(body) {
  const token = await SecureStore.getItemAsync('secure_token');
  const url = `/v2/statedb/tradechannel_yeucc09%24%24p%24f%24e%24c%24group/_find`
  const body2 = {
    ...body,
    limit:10000,
  }
  return SERVER.post(url, body2, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
}

export async function queryDBFU(body) {
  // const token = await SecureStore.getItemAsync('secure_token');
  const token = await SecureStore.getItemAsync('secure_token');
  const url = `/v2/statedb/tradechannel_yeucc09%24%24p%24f%24u%24group/_find`
  const body2 = {
    ...body,
    limit:10000,
  }
  return SERVER.post(url, body2, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
}

export async function postPrivateCC(funcName, param, collection = "") {
  const token = await SecureStore.getItemAsync('secure_token');
  const url = `/pvtchaincode/${funcName}`
  const body = {
    ccversion: CCVERSION,
    args: [],
    pvtArgs: param,
    collection,
  }
  return SERVER.post(url, body, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
}

export const API_ETHERSCAN = axios.create({
  baseURL: ETHERSCAN_API_END_POINT,
  timeout: 60000,
  headers: {
    "Content-Type": "application/json",
    Accept: "application/json",
  },
})

export async function convertToken(funcName, param) {
  const token = await SecureStore.getItemAsync('secure_token');
  const url = `/v2/convertToken/${funcName}`
  return API.post(url, param, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
}

export default API
