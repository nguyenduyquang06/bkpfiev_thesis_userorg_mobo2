import API, { queryCC, postCC, queryChannelDB, serverGet } from "./base";
import _ from "lodash";
import { SecureStore } from "expo";

export async function getUserID() {
  try {
    const { data } = await queryCC("getUserID");
    await SecureStore.setItemAsync("user_id", data.message);
    return data;
  } catch (e) {
    throw e;
  }
}

export async function getUserInfo() {
  try {
    const {
      data: { success, user }
    } = await serverGet("/v2/userInfo");
    if (!success) {
      alert(" Get user info fail ");
    }    
    const attrs = _.reduce(
      user.attrs,
      (res, obj) => ({
        ...res,
        [obj.name]: obj.value
      }),
      {}
    );
    return {
      success,
      user,
      attrs
    };
  } catch (err) {
    throw err;
  }
}

export async function updateUser(param) {  
  const token = await SecureStore.getItemAsync('secure_token');
  const url = `/v2/updateUser`
  const resp = await API.post(url, param, {
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "application/json",
    },
  })
  return resp.data
}