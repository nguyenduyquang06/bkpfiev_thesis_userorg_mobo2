import { queryCC, postCC,queryChannelDB } from "./base";
import _ from "lodash"

export async function getSmartFarmByCropID(redirect, cropID) {
  try {
    const {
      data: { message, success }
    } = await queryCC("getSmartFarmByCropID", cropID);
    if (success) {
      return {
        success,
        data: JSON.parse(message)
      };
    }
    return {
      success
    };
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}

export async function initSmartFarm(
  redirect,
  cropID,
  blockID,
  fromDate,
  topLeftLat,
  topLeftLong,
  topRightLat,
  topRightLong,
  bottomRightLat,
  bottomRightLong,
  bottomLeftLat,
  bottomLeftLong
) {
  try {
    const {
      data: { success }
    } = await postCC(
      "initSmartFarm",
      cropID,
      blockID,
      fromDate,
      topLeftLat,
      topLeftLong,
      topRightLat,
      topRightLong,
      bottomRightLat,
      bottomRightLong,
      bottomLeftLat,
      bottomLeftLong
    );
    return success;
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}

export async function createNode(redirect, cropID, blockID, nodeID) {
  try {
    const {
      data: { success }
    } = await postCC("createNode", cropID, blockID, nodeID);
    return success;
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}

export async function updateSmartFarmToDate(redirect, cropID, blockID, toDate) {
  try {
    const {
      data: { success }
    } = await postCC("updateSmartFarmToDate", cropID, blockID, toDate);
    return success;
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}

export async function updateStage(redirect, cropID, blockID, stage) {
  try {
    const {
      data: { success }
    } = await postCC("updateStage", cropID, blockID, stage);
    return success;
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}

export async function getHistoryNode(redirect, cropID, blockID, nodeID) {
  try {
    const {
      data: { message, success }
    } = await queryCC("getHistoryNode", cropID, blockID, nodeID);
    if (success) {
      return {
        success,
        data: JSON.parse(message)
      };
    }
    return {
      success
    };
  } catch (e) {
    redirect.navigate("Login");
    throw e;
  }
}

export async function getNodeIDList({cropID,blockID}) {
  try {
    let resp = {};
    const body = {
      selector: {
        docType: "SmartFarm",
        cropID,blockID
      }      
    };
    resp = await queryChannelDB(body);    
    return _.get(resp.data.data.docs[0],"NodeIDList");
  } catch (e) {
    throw e;
  }
}