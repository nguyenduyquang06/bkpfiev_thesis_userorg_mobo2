import React from "react";
import { createStackNavigator, createDrawerNavigator } from "react-navigation";
import { Root } from "native-base";
import { Dimensions } from "react-native";
import { SecureStore } from "expo";
const deviceWidth = Dimensions.get("window").width;

import Login from "./container/LoginContainer";
import Sidebar from "./container/SidebarContainer";
import QRScan from "./container/QRScanContainer";
import ProductInformation from "./container/ProductInformationContainer";
import User from "./container/UserContainer";
import ReviewHistory from "./container/ReviewHistoryContainer";
import Checkout from "./container/CheckoutContainer";
import Refund from "./container/RefundContainer";
import TrackingPath from "./container/TrackingPathContainer";
import RechargeBKC from "./container/RechargeBKCContainer";
import NodesInfo from "./container/NodesInfoContainer";
import UpdateSensorHistory from "./container/UpdateSensorHistoryContainer";

const Drawer = createDrawerNavigator(
  {
    Home: { screen: QRScan },
    ReviewHistory: { screen: ReviewHistory },    
    User: { screen: User },
  },
  {
    drawerWidth: deviceWidth - 100,
    drawerPosition: "left",
    contentComponent: props => <Sidebar {...props} />
  }
);

const App = createStackNavigator(
  {
    Login: { screen: Login },
    Drawer: { screen: Drawer },
    Checkout: { screen: Checkout },
    Refund: { screen: Refund },
    QRScan: { screen: QRScan },
    TrackingPath: { screen: TrackingPath },    
    NodesInfo: { screen: NodesInfo },
    UpdateSensorHistory: { screen: UpdateSensorHistory },
    RechargeBKC: { screen: RechargeBKC },    
    ProductInformation: { screen: ProductInformation }
  },
  {
    initialRouteName: "Login",
    headerMode: "none"
  }
);

export default () => (
  <Root>
    <App />
  </Root>
);

