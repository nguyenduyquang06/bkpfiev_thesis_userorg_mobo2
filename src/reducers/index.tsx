import { combineReducers } from "redux";
import { reducer as formReducer } from "redux-form";

import homeReducer from "../container/HomeContainer/reducer";
import dashboardReducer from "../container/DashboardContainer/reducer";
import cropsReducer from "../container/CropsContainer/reducer";
import userReducer from "../container/UserContainer/reducer";

export default combineReducers({
  form: formReducer,
  homeReducer,
  dashboardReducer,
  cropsReducer,
  userReducer,
});
