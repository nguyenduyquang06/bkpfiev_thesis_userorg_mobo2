const initialState = {
  userInfo: {},
  bkcBalance: {},
  ethBalance: {},
};

export default function(state = initialState, action) {
  const { userInfo, bkcBalance, ethBalance, type } = action;

  switch (type) {
    case "FETCH_USER_INFO_SUCCESS":
      return {
        ...state,
        userInfo
      };
    case "FETCH_BKC_BALANCE_SUCCESS":
      return {
        ...state,
        bkcBalance
      };
    case "FETCH_ETH_BALANCE_SUCCESS":
      return {
        ...state,
        ethBalance
      }; 
    case "FETCH_USER_INFO_FAIL":
      return {
        ...state,
        userInfo:{},
      }; 
    case "FETCH_BKC_BALANCE_FAIL":
      return {
        ...state,
        bkcBalance:{},
      }; 
    case "FETCH_ETH_BALANCE_FAIL":
      return {
        ...state,
        ethBalance:{},
      }; 
    case "RESET_USER_SUCCESS":
      return initialState
    default:
      return state;
  }
}
