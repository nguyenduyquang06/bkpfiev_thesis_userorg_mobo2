import { StyleSheet } from "react-native";

const styles: any = StyleSheet.create({
  container: {
    backgroundColor: "#FBFAFA"
  },
  row: {
    flex: 1,
    alignItems: "center"
  },
  text: {
    fontSize: 20,
    marginBottom: 15,
    alignItems: "center"
  },
  value: {
    fontSize: 17,
    paddingLeft: 10,
    color: "#215732"
  },
  key: {
    fontWeight: "bold",
    fontSize: 17,
    // paddingLeft: 10,
    color: "#215732"
  },
  mt: {
    marginTop: 18
  },
  contentView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  }
});
export default styles;
