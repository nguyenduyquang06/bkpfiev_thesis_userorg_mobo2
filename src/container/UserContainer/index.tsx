import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userAction from "./actions";
import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Left,
  Body,
  Right,
  Icon,
  Tabs,
  Tab,
  View
} from "native-base";
import moment from "moment";
import { GENERAL_INFO, BKC_BALANCE_INFO, ETH_BALANCE_INFO } from "./userInfo";
import * as ReactNativeElement from "react-native-elements";
import { Dimensions, Alert } from "react-native";
import { createWallet } from "../../service/wallet";
import { updateUser } from "../../service/user";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import _ from "lodash";
import styles from "./styles";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;
// import {LinearGradient} from "./LinearGradient";
export interface Props {
  navigation: any;
  actions: object;
}

export interface State {}
class UserContainer extends React.Component<Props, State> {
  state = {
    selectedTab: 0,
    password: "",
    confirmPW: "",
    ethAddress: ""
  };
  componentDidMount() {
    const { actions, userInfo } = this.props;
    actions.getBKCBalance(userInfo.userID);
  }

  async onSubmitCreateWallet() {
    const { password } = this.state;
    const { actions, userInfo } = this.props;
    const resp = await createWallet(password);
    if (resp.success) {
      alert("Create wallet successfully");
      return;
    }
    alert("Create wallet fail due to " + resp.message);
    actions.getBKCBalance(userInfo.userID);
  }

  async onSubmitCreateETHWallet() {
    Alert.alert(
      "Confirm",
      "Are you sure you want submit ?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: async () => {
            const { ethAddress } = this.state;
            const { actions } = this.props;
            const param = {
              attrs: {
                ethAddress
              }
            };
            const resp = await updateUser(param);
            if (resp.success) {
              alert("Update user information successfully");
              return;
            }
            alert("Update user information fail");
            actions.getUserInfo();
            actions.getETHBalance(ethAddress);
          }
        }
      ],
      { cancelable: true }
    );
  }
  render() {
    const { userInfo, actions, bkcBalance, ethBalance } = this.props;
    const { selectedTab, password, confirmPW, ethAddress } = this.state;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.openDrawer()}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Title>User Information</Title>
          </Body>
          <Right />
        </Header>
        <Tabs
          locked
          page={selectedTab}
          onChangeTab={({ i }) => {
            this.setState({ selectedTab: i });
            switch (i) {
              case 0:
                actions.getUserInfo();
                return;
              case 1:
                actions.getBKCBalance(userInfo.userID);
                return;
              case 2:
                actions.getETHBalance(userInfo.ethAddress);
                return;
            }
          }}
        >
          <Tab heading="User">
            <Content padder>
              <Text style={{ ...styles.key, fontSize: 30 }}>
                General Information
              </Text>
              {_.map(GENERAL_INFO, (o, key) => (
                <View
                  key={o.label}
                  padder
                  style={{ flexDirection: "row", flexWrap: "wrap" }}
                >
                  {/* <IconMaterial size={30} name="leaf" /> */}
                  <Text style={styles.key}>{o.label}</Text>
                  <Text style={styles.value}>{_.get(userInfo, key)}</Text>
                </View>
              ))}
            </Content>
          </Tab>
          <Tab heading="BKC Balance">
            {_.isEmpty(bkcBalance) ? (
              <View style={styles.contentView}>
                <ReactNativeElement.ThemeProvider
                  theme={{
                    Input: {
                      containerStyle: {
                        width: SCREEN_WIDTH - 50
                      },
                      inputContainerStyle: {
                        borderRadius: 40,
                        borderWidth: 1,
                        borderColor: "rgba(110, 120, 170, 1)",
                        marginVertical: 10
                      },
                      placeholderTextColor: "rgba(110, 120, 170, 1)",
                      inputStyle: {
                        marginLeft: 10,
                        color: "white"
                      },
                      keyboardAppearance: "light",
                      blurOnSubmit: false
                    }
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "rgba(46, 50, 72, 1)",
                      width: SCREEN_WIDTH,
                      alignItems: "center",
                      paddingBottom: 30
                    }}
                  >
                    <Text
                      style={{
                        color: "white",
                        fontSize: 30,
                        marginVertical: 10,
                        fontWeight: "300"
                      }}
                    >
                      Create Wallet
                    </Text>
                    <ReactNativeElement.Input
                      leftIcon={
                        <ReactNativeElement.Icon
                          name="user"
                          type="simple-line-icon"
                          color="rgba(110, 120, 170, 1)"
                          size={25}
                        />
                      }
                      placeholder="Address"
                      autoCapitalize="none"
                      value={userInfo.userID}
                      autoCorrect={false}
                      keyboardType="email-address"
                      returnKeyType="next"
                      ref={input => (this.usernameInput = input)}
                      onSubmitEditing={() => {
                        this.email2Input.focus();
                      }}
                    />
                    <ReactNativeElement.Input
                      leftIcon={
                        <ReactNativeElement.Icon
                          name="lock"
                          type="simple-line-icon"
                          color="rgba(110, 120, 170, 1)"
                          size={25}
                        />
                      }
                      placeholder="Password"
                      autoCapitalize="none"
                      secureTextEntry={true}
                      autoCorrect={false}
                      keyboardType="default"
                      onChangeText={text => this.setState({ password: text })}
                      returnKeyType="next"
                      ref={input => (this.password2Input = input)}
                      onSubmitEditing={() => {
                        this.confirmPassword2Input.focus();
                      }}
                    />
                    <ReactNativeElement.Input
                      leftIcon={
                        <ReactNativeElement.Icon
                          name="lock"
                          type="simple-line-icon"
                          color="rgba(110, 120, 170, 1)"
                          size={25}
                        />
                      }
                      placeholder="Confirm Password"
                      autoCapitalize="none"
                      keyboardAppearance="light"
                      secureTextEntry={true}
                      autoCorrect={false}
                      keyboardType="default"
                      onChangeText={text => this.setState({ confirmPW: text })}
                      returnKeyType="done"
                      ref={input => (this.confirmPassword2Input = input)}
                      blurOnSubmit
                    />
                    <ReactNativeElement.Button
                      title="Create Wallet"
                      titleStyle={{ fontWeight: "bold", fontSize: 18 }}
                      linearGradientProps={{
                        colors: ["#FF9800", "#F44336"],
                        start: [1, 0],
                        end: [0.2, 0]
                      }}
                      buttonStyle={{
                        borderWidth: 0,
                        borderColor: "transparent",
                        borderRadius: 20
                      }}
                      containerStyle={{
                        marginVertical: 10,
                        height: 40,
                        width: 200
                      }}
                      icon={{
                        name: "arrow-right",
                        type: "font-awesome",
                        size: 15,
                        color: "white"
                      }}
                      onPress={e => {
                        if (
                          password === "" ||
                          confirmPW === "" ||
                          userInfo.userID === ""
                        ) {
                          {
                            alert("Please fill in all require fields");
                          }
                        }
                        if (password !== confirmPW) {
                          alert("Password mismatch");
                          return;
                        }
                        alert("Creating wallet . . .");
                        this.onSubmitCreateWallet();
                      }}
                      iconRight
                      iconContainerStyle={{ marginLeft: 10, marginRight: -10 }}
                    />
                  </View>
                </ReactNativeElement.ThemeProvider>
              </View>
            ) : (
              <Content padder>
                <Text style={{ ...styles.key, fontSize: 30 }}>
                  BachKhoaCrypto Balance
                </Text>
                {_.map(BKC_BALANCE_INFO, (o, key) => (
                  <View
                    key={o.label}
                    padder
                    style={{ flexDirection: "row", flexWrap: "wrap" }}
                  >
                    {/* <IconMaterial size={30} name="leaf" /> */}
                    <Text style={styles.key}>{o.label}</Text>
                    <Text style={styles.value}>
                      {o.value ? o.value : _.get(bkcBalance, key)}
                    </Text>
                  </View>
                ))}
              </Content>
            )}
          </Tab>
          <Tab heading="ETH Balance">
            {_.isEmpty(_.get(userInfo, "ethAddress")) ? (
              <View style={styles.contentView}>
                <ReactNativeElement.ThemeProvider
                  theme={{
                    Input: {
                      containerStyle: {
                        width: SCREEN_WIDTH - 50
                      },
                      inputContainerStyle: {
                        borderRadius: 40,
                        borderWidth: 1,
                        borderColor: "rgba(110, 120, 170, 1)",
                        marginVertical: 10
                      },
                      placeholderTextColor: "rgba(110, 120, 170, 1)",
                      inputStyle: {
                        marginLeft: 10,
                        color: "white"
                      },
                      keyboardAppearance: "light",
                      blurOnSubmit: false
                    }
                  }}
                >
                  <View
                    style={{
                      backgroundColor: "rgba(46, 50, 72, 1)",
                      width: SCREEN_WIDTH,
                      alignItems: "center",
                      paddingBottom: 30
                    }}
                  >
                    <Text
                      style={{
                        color: "white",
                        fontSize: 30,
                        marginVertical: 10,
                        fontWeight: "300"
                      }}
                    >
                      CREATE ETH Wallet
                    </Text>
                    <ReactNativeElement.Input
                      leftIcon={
                        <ReactNativeElement.Icon
                          name="lock"
                          type="simple-line-icon"
                          color="rgba(110, 120, 170, 1)"
                          size={25}
                        />
                      }
                      placeholder="Confirm Password"
                      autoCapitalize="none"
                      keyboardAppearance="light"
                      autoCorrect={false}
                      keyboardType="default"
                      onChangeText={text => this.setState({ ethAddress: text })}
                      returnKeyType="done"
                      blurOnSubmit
                    />
                    <ReactNativeElement.Button
                      title="Create ETH Wallet"
                      titleStyle={{ fontWeight: "bold", fontSize: 18 }}
                      linearGradientProps={{
                        colors: ["#FF9800", "#F44336"],
                        start: [1, 0],
                        end: [0.2, 0]
                      }}
                      buttonStyle={{
                        borderWidth: 0,
                        borderColor: "transparent",
                        borderRadius: 20
                      }}
                      containerStyle={{
                        marginVertical: 10,
                        height: 40,
                        width: 200
                      }}
                      icon={{
                        name: "arrow-right",
                        type: "font-awesome",
                        size: 15,
                        color: "white"
                      }}
                      onPress={e => {
                        if (ethAddress === "") {
                          {
                            alert("Please fill in all require fields");
                          }
                        }
                        alert("Updating ETH wallet . . .");
                        this.onSubmitCreateETHWallet();
                      }}
                      iconRight
                      iconContainerStyle={{ marginLeft: 10, marginRight: -10 }}
                    />
                  </View>
                </ReactNativeElement.ThemeProvider>
              </View>
            ) : (
              <Content padder>
                <Text style={{ ...styles.key, fontSize: 30 }}>ETH Balance</Text>
                {_.map(ETH_BALANCE_INFO, (o, key) => (
                  <View
                    key={o.label}
                    padder
                    style={{ flexDirection: "row", flexWrap: "wrap" }}
                  >
                    {/* <IconMaterial size={30} name="leaf" /> */}
                    <Text style={styles.key}>{o.label}</Text>
                    <Text style={styles.value}>
                      {o.value
                        ? o.value
                        : key === "balance"
                        ? parseInt(_.get(ethBalance, key), 10) /
                            Math.pow(10, 18) +
                          " ETH"
                        : _.get(ethBalance, key)}
                    </Text>
                  </View>
                ))}
                <Button
                  onPress={() => {
                    this.props.navigation.navigate("RechargeBKC", {});
                  }}
                  style={{ alignSelf: "center" }}
                  primary
                >
                  <Text>RECHARGE BKC</Text>
                </Button>
              </Content>
            )}
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ ...userAction }, dispatch)
  };
};

const mapStateToProps = state => ({
  userInfo: state.userReducer.userInfo,
  bkcBalance: state.userReducer.bkcBalance,
  ethBalance: state.userReducer.ethBalance
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserContainer);
