export const GENERAL_INFO = {
  fullName: {
    label: "Full name",
  },
  address: {
    label: "Address",
  },
  active: {
    label: "Active",
  },
  email: {
    label: "Email",
  },
  city: {
    label: "City",
  },
  zipCode: {
    label: "Zip Code",
  },
  state: {
    label: "State",
  },
  country: {
    label: "Country",
  },
  role: {
    label: "Role",
  },
  userID: {
    label: "User ID",
  },
}

export const ETH_BALANCE_INFO = {
  address: {
    label: "Ethereum Address:",
  },
  balance: {
    label: "Ether Balance",
  },
  testNet: {
    label: "Test Net",
    value: "Ropsten",
  },
  nodeInfo: {
    label: "Node URL",
    value: "https://ropsten.infura.io/OuE2BVwDYYh4Iucxac4d",
  },
}

export const BKC_BALANCE_INFO = {
  address: {
    label: "BKC Address:",
  },
  balance: {
    label: "BKC Balance",
  },
  testNet: {
    label: "Test Net",
    value: "Internal",
  },
  nodeInfo: {
    label: "Node URL",
    value: "http://statedb.bkpfiev.tk/carrierorg/",
  },
}
