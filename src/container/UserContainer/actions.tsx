import * as API from "../../service/user";
import * as API2 from "../../service/wallet";
import { getEthBalance } from "../../service/ethereum";
export function getUserInfo() {
  return async dispatch => {
    const { success, attrs } = await API.getUserInfo();
    const { message } = await API.getUserID();
    if (!success) {
      alert("Get user info fail, you might want check the connection");
      return dispatch({
        type: "FETCH_USER_INFO_FAIL"
      });
      return;
    }
    return dispatch({
      type: "FETCH_USER_INFO_SUCCESS",
      userInfo: {
        ...attrs,
        userID: message
      }
    });
  };
}

export function getBKCBalance(userID) {
  return async dispatch => {
    const resp = await API2.getWalletBalance(userID);
    if (resp.length === 0) {
      return dispatch({
        type: "FETCH_BKC_BALANCE_FAIL"
      });
    }
    return dispatch({
      type: "FETCH_BKC_BALANCE_SUCCESS",
      bkcBalance: resp[0]
    });
  };
}

export function getETHBalance(ethAddress) {
  return async dispatch => {
    const { result, status } = await getEthBalance(ethAddress);
    console.log(ethAddress);
    if (status === "1") {
      return dispatch({
        type: "FETCH_ETH_BALANCE_SUCCESS",
        ethBalance: {
          address: ethAddress,
          balance: result
        }
      });
    }
    return dispatch({
      type: "FETCH_ETH_BALANCE_FAIL"
    });
  };
}

export function resetUser() {
  return dispatch => {
    return dispatch({
      type: "RESET_USER_SUCCESS"
    });
  };
}
