import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as cropsAction from "./actions";
import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Left,
  Body,
  Right,
  Picker,
  Icon,
  Thumbnail,
  CardItem,
  Card,
  Label,
  DeckSwiper,
  View
} from "native-base";
import moment from "moment";
import { Alert } from "react-native";
import { SecureStore, MapView } from "expo";
import * as Progress from "react-native-progress";
import { getContract, acceptContract } from "../../service/contract";
import { getSmartFarmByCropID } from "../../service/smartFarm";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import _ from "lodash";
import styles from "./styles";

const mappingStatusToValue = {
  contractFarmerExporter: {
    REQUEST_FROM_EXPORTER: 25,
    ACCEPT_REQUEST_TRADE_STEP_ONE_AND_SHIPPING: 50,
    CARRIER_DONE_SHIPPING: 75,
    DONE_SUCCESSFULLY: 100
  },
  contractExporterImporter: {
    REQUEST_STEP_TWO_FROM_IMPORTER: 20,
    READY_SHIPPING_STEP_TWO: 40,
    SHIPPING_EXPORTER_IMPORTER: 60,
    CARRIER_DONE_SHIPPING: 80,
    DONE_SUCCESSFULLY: 100
  },
  contractImporterFactory: {
    REQUEST_STEP_THREE_FROM_FACTORY: 25,
    ACCEPT_REQUEST_TRADE_STEP_THREE_AND_SHIPPING: 50,
    CARRIER_DONE_SHIPPING: 75,
    DONE_SUCCESSFULLY: 100
  }
};

export interface Props {
  navigation: any;
  actions: object;
}

export interface State {
  contractList: object[];
}
class ContractContainer extends React.Component<Props, State> {
  state = {
    contractList: []
  };

  async componentDidMount() {
    const contractList = await getContract();
    console.log(contractList);
    this.setState({
      contractList
    });
  }

  async onClickAcceptContract(contractID) {
    // const resp = await acceptContract(contractID)
    Alert.alert(
      "Confirm",
      "Are you sure you want submit ?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: async () => {
            const resp = await acceptContract(
              this.props.navigation,
              contractID
            );
            if (resp.success) {
              this.componentDidMount();
              return alert(
                "Accept trade successfully at transaction " + resp.transactionID
              );
            } else {
              return alert("Accept trade batch fail cause of " + resp.message);
            }
          }
        }
      ],
      { cancelable: true }
    );
  }

  render() {
    const { contractList } = this.state;
    const { actions } = this.props;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.openDrawer()}
              />
            </Button>
          </Left>
          <Body>
            <Title>Contract</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <View
            style={{
              justifyContent: "center",
              flexDirection: "row",
              flexWrap: "wrap"
            }}
          >
            <Text>Filter</Text>
            <Picker
              mode="dropdown"
              placeholder="Select crop"
              iosIcon={<Icon name="arrow-down" />}
              textStyle={{ color: "#5cb85c" }}
              itemTextStyle={{ color: "#788ad2" }}
              style={{
                width: 150,
                alignSelf: "center",
                alignItems: "flex-end"
              }}
              // selectedValue={selectedCrop}
              // onValueChange={this.onValueChange.bind(this)}
            >
              <Picker.Item label={"None"} value={"None"} />
            </Picker>
          </View>
          {_.map(contractList, o => (
            <Card key={o.contractID}>
              <CardItem header bordered>
                <Text>{o.contractID}</Text>
                {o.status === "REQUEST_FROM_EXPORTER" ? (
                  <Right style={{ marginRight: -50 }}>
                    <Button
                      onPress={() => {
                        this.onClickAcceptContract(o.contractID);
                      }}
                      transparent
                      textStyle={{ color: "#87838B" }}
                    >
                      <IconMaterial size={30} color="#0051ff" name="check" />
                    </Button>
                  </Right>
                ) : null}
              </CardItem>
              <CardItem bordered>
                <Body>
                  <Text>Crop: {o.cropID} </Text>
                  <Text>Block: {o.blockID} </Text>
                  <Text>Batch: {o.batchID}</Text>
                  <Text>Progress: {o.status}</Text>
                  <Progress.Bar
                    progress={
                      _.get(
                        mappingStatusToValue.contractFarmerExporter,
                        o.status
                      ) / 100
                    }
                    width={280}
                    height={18}
                  />
                </Body>
              </CardItem>
              <CardItem header>
                <Right style={{ marginRight: -70 }}>
                  <Button
                    onPress={() => {
                      console.log("test");
                    }}
                    transparent
                    textStyle={{ color: "#87838B" }}
                  >
                    <Icon
                      style={{ fontSize: 30 }}
                      name="ios-information-circle"
                    />
                    <Text>Details</Text>
                  </Button>
                </Right>
              </CardItem>
            </Card>
          ))}
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ ...cropsAction }, dispatch)
  };
};

const mapStateToProps = state => ({
  selectedCrop: state.cropsReducer.selectedCrop,
  selectedSmartFarm: state.cropsReducer.selectedSmartFarm,
  smartFarmList: state.cropsReducer.smartFarmList,
  cropList: state.cropsReducer.cropList
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContractContainer);
