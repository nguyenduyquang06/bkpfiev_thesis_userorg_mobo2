import * as React from "react";
import TrackingPath from "../../stories/screens/TrackingPath";
export interface Props {
	navigation: any,
}
export interface State {}
export default class TrackingPathContainer extends React.Component<Props, State> {
	render() {
		return <TrackingPath navigation={this.props.navigation} />;
	}
}
