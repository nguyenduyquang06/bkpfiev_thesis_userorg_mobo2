const initialState = {
	list: [],
	isLoading: true,
};

export default function(state = initialState, action) {
	const {list, isLoading} = action
	if (action.type === "FETCH_LIST_SUCCESS") {
		return {
			...state,
			list: list,
		};
	}
	if (action.type === "LIST_IS_LOADING") {
		return {
			...state,
			isLoading: isLoading,
		};
	}
	return state;
}
