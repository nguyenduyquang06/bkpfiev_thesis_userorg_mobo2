import * as React from "react";
import CropDetails from "../../stories/screens/CropDetails";
export interface Props {
	navigation: any,
}
export interface State {}
export default class CropDetailsContainer extends React.Component<Props, State> {
	render() {
		return <CropDetails navigation={this.props.navigation} />;
	}
}
