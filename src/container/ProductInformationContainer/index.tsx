import * as React from "react";
import ProductInformation from "../../stories/screens/ProductInformation";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ProductInformationContainer extends React.Component<Props, State> {
	render() {
		return <ProductInformation navigation={this.props.navigation} />;
	}
}
