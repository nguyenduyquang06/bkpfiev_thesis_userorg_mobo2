import * as React from "react";
import CropCreate from "../../stories/screens/CropCreate";
export interface Props {
	navigation: any,
}
export interface State {}
export default class CropCreateContainer extends React.Component<Props, State> {
	render() {
		return <CropCreate navigation={this.props.navigation} />;
	}
}
