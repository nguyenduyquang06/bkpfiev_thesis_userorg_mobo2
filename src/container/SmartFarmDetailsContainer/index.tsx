import * as React from "react";
import SmartFarmDetails from "../../stories/screens/SmartFarmDetails";
export interface Props {
	navigation: any,
}
export interface State {}
export default class SmartFarmDetailsContainer extends React.Component<Props, State> {
	render() {
		return <SmartFarmDetails navigation={this.props.navigation} />;
	}
}
