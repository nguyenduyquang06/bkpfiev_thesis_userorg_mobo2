import * as React from "react";
import CoffeeBatchCreate from "../../stories/screens/CoffeeBatchCreate";
export interface Props {
	navigation: any,
}
export interface State {}
export default class CoffeeBatchCreateContainer extends React.Component<Props, State> {
	render() {
		return <CoffeeBatchCreate navigation={this.props.navigation} />;
	}
}
