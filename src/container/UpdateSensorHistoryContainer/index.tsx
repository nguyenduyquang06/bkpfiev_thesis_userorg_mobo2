import * as React from "react";
import UpdateSensorHistory from "../../stories/screens/UpdateSensorHistory";
export interface Props {
	navigation: any,
}
export interface State {}
export default class UpdateSensorHistoryContainer extends React.Component<Props, State> {
	render() {
		return <UpdateSensorHistory navigation={this.props.navigation} />;
	}
}
