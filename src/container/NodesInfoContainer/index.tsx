import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Left,
  Body,
  Right,
  Input,
  Icon,
  Thumbnail,
  CardItem,
  Card,
  Label,
  DeckSwiper,
  Item,
  View
} from "native-base";
import moment from "moment";
import { MapView } from "expo";
import { getHistoryNode } from "../../service/smartFarm";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import _ from "lodash";
import styles from "./styles";

const BKLOGO = require("../../../assets/bk_logo.png");

export interface Props {
  navigation: any;
  actions: object;
}

export interface State {
  mapType: boolean;
  smartFarmList: object[];
  cropList: object[];
  nodeHistory: object[];
  nodeHistoryMean: object[];
  selectedNodeID: string;
  current: object;
  size: number;
}

function calcMean(nodeHistory, size = 10) {
  const nodeHistoryCut = _.take(nodeHistory, size);
  const sensorHistoryList = _.map(nodeHistoryCut, (value, key) => {
    return value.node.sensor;
  });
  const latMean = _.meanBy(sensorHistoryList, o => o.latitude);
  const longMean = _.meanBy(sensorHistoryList, o => o.longitude);
  const temperatureMean = _.meanBy(sensorHistoryList, o => o.temperature);
  const humidityMean = _.meanBy(sensorHistoryList, o => o.humidity);
  return {
    latMean,
    longMean,
    temperatureMean,
    humidityMean
  };
}

class NodesInfoContainer extends React.Component<Props, State> {  
  constructor(props) {
    super(props);
    this.state = {
      cropList: [],      
      size: 10,
      smartFarmList: [],
      selectedNodeID: "None",
      nodeHistoryMean: [],
      current: {},
      nodeHistory: [],
      mapType: false // true is satellite, else standard
    };    
  }

  async onValueChange(value) {
    const { coffeeBatch } = this.props.navigation.state.params;
    const { data } = await getHistoryNode(
      this.props.navigation,
      coffeeBatch.cropID,
      coffeeBatch.blockID,
      value,
    );
    if (data.length === 0) {
      return false;
    } else {
      const dataReverse = _.reverse(data);
      this.setState({
        selectedNodeID: value,
        nodeHistory: dataReverse,
        nodeHistoryMean: calcMean(dataReverse),
        current: dataReverse[0].node.sensor
      });
      return true;
    }
  }
  async componentDidMount() {
    let flag = true;
    const { nodeIDList } = this.props.navigation.state.params;
    let i = nodeIDList.length - 1;
    while (flag) {
      const resp = await this.onValueChange(nodeIDList[i--]);
      if (resp) {
        flag = false;
      }
    }
  }

  goBack() {
    const { navigation } = this.props;
    navigation.goBack();
  }

  render() {
    const {
      nodeHistory,
      selectedNodeID,
      nodeHistoryMean,
      current,
    } = this.state;
    const { nodeIDList,coffeeBatch } = this.props.navigation.state.params;    
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>
          <Body style={{ flex: 4 }}>
            <Title>Node Info</Title>
          </Body>
        </Header>
        <View
          style={{
            justifyContent: "center",
            flexDirection: "row",
            flexWrap: "wrap"
          }}
        >
          <Input
            value={this.state.size ? this.state.size.toString() : ""}
            placeholder="Input number of value to get mean"
            onSubmitEditing={() =>
              this.setState({
                nodeHistoryMean: calcMean(
                  this.state.nodeHistory,
                  this.state.size
                )
              })
            }
            onChangeText={text => {
              this.setState({
                size: parseFloat(text.replace(/[^(\d+)\.(\d+)]/g, ""))
              });
            }}
            keyboardType="numeric"
          />
        </View>
        {typeof nodeIDList !== "undefined" && selectedNodeID !== "None" ? (
          <DeckSwiper
            dataSource={nodeIDList}
            onSwipeRight={this.onValueChange.bind(this)}
            onSwipeLeft={this.onValueChange.bind(this)}
            renderItem={o => {
              return (
                <Card key={o} style={{ flex: 0 }}>
                  <CardItem>
                    <Left>
                      <Thumbnail square source={BKLOGO} />
                      <Body>
                        <View
                          padder
                          style={{ flexDirection: "row", flexWrap: "wrap" }}
                        >
                          <Text>{`${coffeeBatch.cropID}-${
                            coffeeBatch.blockID
                          }-${selectedNodeID}`}</Text>
                          <Right>
                            <IconMaterial.Button
                              onPress={() => {
                                this.setState({ mapType: !this.state.mapType });
                              }}
                              color="#0051ff"
                              backgroundColor="transparent"
                              size={30}
                              name="eye"
                            />
                          </Right>
                        </View>
                        <Text note>
                          {" "}
                          {`At ${moment
                            .unix(nodeHistory[0].timestamp)
                            .format("YYYY-MM-DD HH:mm:ss")}`}
                        </Text>                  
                      </Body>
                    </Left>
                  </CardItem>
                  <View>
                    <MapView
                      style={{ alignSelf: "stretch", height: 300 }}
                      liteMode={true}
                      mapType={this.state.mapType ? "satellite" : "standard"}
                      initialRegion={{
                        latitude: nodeHistoryMean.latMean,
                        longitude: nodeHistoryMean.longMean,
                        latitudeDelta: 0.0005,
                        longitudeDelta: 0.0005
                      }}
                    >
                      <MapView.Marker
                        coordinate={{
                          latitude: nodeHistoryMean.latMean,
                          longitude: nodeHistoryMean.longMean
                        }}
                        title={`Node ID: ${selectedNodeID}`}
                        description={`Lat:${nodeHistoryMean.latMean.toPrecision(
                          4
                        )}, Long:${nodeHistoryMean.longMean.toPrecision(4)}, ${
                          current.temperature
                        }*C, ${current.humidity}% `}
                      />
                    </MapView>
                  </View>
                  <CardItem>
                    <Body>                      
                      <Label style={styles.key}>
                        {`Current Value: ${current.temperature}*C, ${
                          current.humidity
                        }%`}
                      </Label>
                      <Label style={styles.key}>
                        {`Avg Value of ${
                          this.state.size
                        } records recent: ${nodeHistoryMean.temperatureMean.toFixed(
                          2
                        )}*C, ${nodeHistoryMean.humidityMean.toFixed(2)}%`}
                      </Label>
                      <Label style={styles.key}>
                        Swipe left or right to next item
                      </Label>
                      <View
                        style={{
                          justifyContent: "center",
                          flexDirection: "row",
                          flexWrap: "wrap"
                        }}
                      >               
                        <Button
                          nodeHistory
                          backgroundColor="#215732"
                          onPress={() => {
                            this.props.navigation.navigate(
                              "UpdateSensorHistory",
                              {
                                nodeHistory
                              }
                            );
                          }}
                          style={{ alignSelf: "center" }}
                          primary
                        >
                          <Text> View History </Text>
                        </Button>
                      </View>
                    </Body>
                  </CardItem>
                </Card>
              );
            }}
          />
        ) : null}
      </Container>
    );
  }
}

export default NodesInfoContainer;
