import * as React from "react";
import QRScan from "../../stories/screens/QRScan";
export interface Props {
	navigation: any,
}
export interface State {}
export default class QRScanContainer extends React.Component<Props, State> {
	render() {
		return <QRScan navigation={this.props.navigation} />;
	}
}
