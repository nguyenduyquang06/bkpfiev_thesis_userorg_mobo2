import * as React from "react";
import SmartFarmCreate from "../../stories/screens/SmartFarmCreate";
export interface Props {
	navigation: any,
}
export interface State {}
export default class SmartFarmCreateContainer extends React.Component<Props, State> {
	render() {
		return <SmartFarmCreate navigation={this.props.navigation} />;
	}
}
