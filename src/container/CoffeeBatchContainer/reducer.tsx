const initialState = {
  // list: [],
  // isLoading: true,
  selectedCrop: {},
  selectedSmartFarm: {},
  smartFarmList: [],
  cropList: [],
};

export default function(state = initialState, action) {
  const {
    selectedCrop,
    selectedSmartFarm,
    smartFarmList,
    cropList,
    type,
  } = action;

  switch (type) {
    case "FETCH_CROP_LIST_SUCCESS":
      return {
        ...state,
        cropList,
      };
    case "FETCH_SMART_FARM_LIST_SUCCESS":
      return {
        ...state,
        smartFarmList,
      };
    case "SELECT_CROP_SUCCESS":
      return {
        ...state,
        selectedCrop,
      };
    case "SELECT_SMART_FARM_SUCCESS":
      return {
        ...state,
        selectedSmartFarm,
      };
    default:
      return state;
  }
}
