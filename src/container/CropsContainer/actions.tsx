export function setSelectedCrop(selectedCrop) {
  return (dispatch) => {   
    return dispatch({
      type: "SELECT_CROP_SUCCESS",
      selectedCrop,
    });
  };
}

export function setSelectedSmartFarm(selectedSmartFarm) {
  return (dispatch) => {   
    return dispatch({
      type: "SELECT_SMART_FARM_SUCCESS",
      selectedSmartFarm,
    });
  };
}