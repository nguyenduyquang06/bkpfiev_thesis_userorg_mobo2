import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as cropsAction from "./actions";
import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Left,
  Body,
  Right,
  Picker,
  Icon,
  Thumbnail,
  CardItem,
  Card,
  Label,
  DeckSwiper,
  View,
} from "native-base";
import moment from "moment";
import { MapView } from "expo";
import { getCropWithUID } from "../../service/crop";
import { getSmartFarmByCropID } from "../../service/smartFarm";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import _ from "lodash";
import styles from "./styles";

const BKLOGO = require("../../../assets/bk_logo.png");

export interface Props {
  navigation: any;
  actions: object;
}

function calcMean(obj) {
  const latLongList = _.map(obj, (value, key) => {
    return value;
  });
  const latMean = _.meanBy(latLongList, (o) => o.Latitude);
  const longMean = _.meanBy(latLongList, (o) => o.Longitude);
  return {
    lat: latMean,
    long: longMean,
  };
}

export interface State {
  mapType: boolean;
  smartFarmList: object[];
  cropList: object[];
}
class CropsContainer extends React.Component<Props, State> {
  state = {
    cropList: [],
    smartFarmList: [],
    mapType: false, // true is satellite, else standard
  };

  async onValueChange(value: { id: string }) {
    const { data } = await getSmartFarmByCropID(
      this.props.navigation,
      value.id,
    );
    const { actions } = this.props;
    actions.setSelectedCrop(value);
    this.setState({
      smartFarmList: data,
    });
  }
  async componentDidMount() {
    const { success, crops } = await getCropWithUID(this.props.navigation);
    if (success) {
      this.setState({
        cropList: crops,
      });
    }
  }

  render() {
    const { smartFarmList } = this.state;
    const { selectedCrop, actions } = this.props;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.openDrawer()}
              />
            </Button>
          </Left>
          <Body>
            <Title>Crops</Title>
          </Body>
          <Right />
        </Header>
        <View
          style={{
            justifyContent: "center",
            flexDirection: "row",
            flexWrap: "wrap",
          }}
        >
          <Picker
            mode="dropdown"
            placeholder="Select crop"
            iosIcon={<Icon name="arrow-down" />}
            textStyle={{ color: "#5cb85c" }}
            itemTextStyle={{ color: "#788ad2" }}
            style={{
              width: 150,
              alignSelf: "center",
              alignItems: "flex-end",
            }}
            selectedValue={selectedCrop}
            onValueChange={this.onValueChange.bind(this)}
          >
            <Picker.Item label={"None"} value={"None"} />
            {_.map(this.state.cropList, (o) => (
              <Picker.Item label={o.id} key={o.id} value={o} />
            ))}
          </Picker>
          <IconMaterial.Button
            onPress={() => {
              this.props.navigation.navigate("CropCreate");
            }}
            color="#0051ff"
            backgroundColor="transparent"
            size={30}
            name="plus-circle"
          />
          <IconMaterial.Button
            onPress={() => {
              this.props.navigation.navigate("SmartFarmCreate");
            }}
            color="#0051ff"
            backgroundColor="transparent"
            size={30}
            name="bandcamp"
          />
          <IconMaterial.Button
            onPress={() => {
              this.props.navigation.navigate("CropDetails");
            }}
            color="#0051ff"
            backgroundColor="transparent"
            size={30}
            name="circle-edit-outline"
          />
        </View>
        {typeof smartFarmList !== "undefined" ? (
          <DeckSwiper
            dataSource={smartFarmList}
            renderItem={(o) => {
              const { long, lat } = calcMean(o.position);
              const latLongList = _.map(o.position, (p, key) => ({
                latitude: p.Latitude,
                longitude: p.Longitude,
              }));
              return (
                <Card key={o.blockID} style={{ flex: 0 }}>
                  <CardItem>
                    <Left>
                      <Thumbnail square source={BKLOGO} />
                      <Body>
                        <View
                          padder
                          style={{ flexDirection: "row", flexWrap: "wrap" }}
                        >
                          <Text>{o.blockID}</Text>
                          <Right>
                            <IconMaterial.Button
                              onPress={() => {
                                this.setState({ mapType: !this.state.mapType });
                              }}
                              color="#0051ff"
                              backgroundColor="transparent"
                              size={30}
                              name="eye"
                            />
                          </Right>
                        </View>
                        <Text note>{`${moment(o.FromDate).format(
                          "DD/MM/YYYY",
                        )} - ${
                          o.ToDate
                            ? moment(o.ToDate).format("DD/MM/YYYY")
                            : "Not yet updated"
                        }`}</Text>
                      </Body>
                    </Left>
                  </CardItem>
                  <View>
                    <MapView
                      style={{ alignSelf: "stretch", height: 300 }}
                      liteMode={true}
                      mapType={this.state.mapType ? "satellite" : "standard"}
                      initialRegion={{
                        latitude: lat,
                        longitude: long,
                        latitudeDelta: 0.005,
                        longitudeDelta: 0.005,
                      }}
                    >
                      <MapView.Polygon
                        coordinates={latLongList}
                        strokeWidth={2}
                        fillOpacity={1}
                        draggable
                        editable
                        strokeColor="#215732"
                        fillColor="#368e52"
                      />
                    </MapView>
                  </View>
                  <CardItem>
                    <Body>
                      <Text>{o.NodeIDList.length} node active</Text>
                      <Text>
                        Lat.{lat.toFixed(6)} , Long.{long.toFixed(6)}
                      </Text>
                      <Label style={styles.key}>
                        Swipe left or right to next item
                      </Label>
                    </Body>
                  </CardItem>
                  <CardItem>
                    <Left>
                      <Button
                        onPress={() => {
                          actions.setSelectedSmartFarm(o);
                          this.props.navigation.navigate("SmartFarmDetails", {
                            lat,
                            long,
                            latLongList,
                          });
                        }}
                        transparent
                        textStyle={{ color: "#87838B" }}
                      >
                        <Icon
                          style={{ fontSize: 50 }}
                          name="ios-information-circle"
                        />
                        <Text>Details</Text>
                      </Button>
                    </Left>
                    <Right>
                      <Button
                        transparent
                        onPress={() => {
                          actions.setSelectedSmartFarm(o);
                          this.props.navigation.navigate("NodesInfo", {
                            nodeIDList: o.NodeIDList,
                          });
                        }}
                        textStyle={{ color: "#87838B" }}
                      >
                        <IconMaterial
                          size={50}
                          color="#0051ff"
                          name="resistor-nodes"
                        />
                        <Text>View Node Info</Text>
                      </Button>
                    </Right>
                  </CardItem>
                </Card>
              );
            }}
          />
        ) : null}
      </Container>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators({ ...cropsAction }, dispatch),
  };
};

const mapStateToProps = (state) => ({
  selectedCrop: state.cropsReducer.selectedCrop,
  selectedSmartFarm: state.cropsReducer.selectedSmartFarm,
  smartFarmList: state.cropsReducer.smartFarmList,
  cropList: state.cropsReducer.cropList,
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CropsContainer);
