import * as React from "react";
import ReviewHistory from "../../stories/screens/ReviewHistory";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ReviewHistoryContainer extends React.Component<Props, State> {
	render() {
		return <ReviewHistory navigation={this.props.navigation} />;
	}
}
