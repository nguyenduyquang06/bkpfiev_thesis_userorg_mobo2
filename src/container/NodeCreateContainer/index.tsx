import * as React from "react";
import NodeCreate from "../../stories/screens/NodeCreate";
export interface Props {
	navigation: any,
}
export interface State {}
export default class NodeCreateContainer extends React.Component<Props, State> {
	render() {
		return <NodeCreate navigation={this.props.navigation} />;
	}
}
