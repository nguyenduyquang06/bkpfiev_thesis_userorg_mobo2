import * as React from "react";
import Refund from "../../stories/screens/Refund";
export interface Props {
	navigation: any,
}
export interface State {}
export default class RefundContainer extends React.Component<Props, State> {
	render() {
		return <Refund navigation={this.props.navigation} />;
	}
}
