import * as React from "react";
import DrawPolygonMap from "../../stories/screens/DrawPolygonMap";
export interface Props {
	navigation: any,
}
export interface State {}
export default class DrawPolygonMapContainer extends React.Component<Props, State> {
	render() {
		return <DrawPolygonMap navigation={this.props.navigation} />;
	}
}
