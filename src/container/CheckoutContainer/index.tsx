import * as React from "react";
import Checkout from "../../stories/screens/Checkout";
export interface Props {
	navigation: any,
}
export interface State {}
export default class CheckoutContainer extends React.Component<Props, State> {
	render() {
		return <Checkout navigation={this.props.navigation} />;
	}
}
