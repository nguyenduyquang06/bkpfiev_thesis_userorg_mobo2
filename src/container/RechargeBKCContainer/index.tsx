import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Body,
  View,
  Text,
  Button,
  List,
  ListItem,
  Right,
  Input
} from "native-base";
import { connect } from "react-redux";
import styles from "./styles";
import _ from "lodash";
import moment from "moment";
import {
  getSystemETHAddress,
  getRateETHBKC,
  convertBKCToETH
} from "../../service/wallet";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { Alert } from "react-native";

export interface Props {
  navigation: any;
}

export interface State {}
class RechargeBKCContainer extends React.Component<Props, State> {
  state = {
    ethSysAdd: "NOT AVAILABLE",
    rate: "NOT AVAILABLE",
    inputETH: null,
    privKey: ""
  };

  async componentDidMount() {
    const resp = await getSystemETHAddress();
    const rateResp = await getRateETHBKC();
    const { userInfo } = this.props;
    if (resp.success && rateResp.success) {
      this.setState({
        ethSysAdd: resp.message,
        rate: rateResp.message
      });
      if (userInfo.fullName === "Nguyen Duy Quang") {
        this.setState({
          privKey:
            "D4B0FC84B6D7CCDFD78059DF3D6D951F8B968C4D25293CC37846F0C550BF34C2"
        });
      }
    } else {
      alert("Cannot get ETH SYSTEM ADDRESS");
    }
  }

  async onSubmitRechargeBKC() {
    Alert.alert(
      "Confirm",
      "Are you sure you want submit ?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: async () => {
            const { ethBalance } = this.props;
            const { privKey, inputETH } = this.state;
            if (inputETH.toString === "" || privKey === "") {
              alert("Please fill in required fields first");
            }
            const payload = {
              fromAddress: ethBalance.address,
              fromPrivateKey: privKey,
              value: inputETH.toString()
            };
            alert("Loading.....");
            const resp = await convertBKCToETH(payload);
            if (resp.success) {
              alert("Transaction " + resp.transactionID + " success");
            } else {
              alert(
                "Transaction " +
                  resp.transactionID +
                  " fail due to " +
                  resp.message
              );
            }
          }
        }
      ],
      { cancelable: true }
    );
  }

  render() {
    const { bkcBalance, userInfo, ethBalance } = this.props;
    const { ethSysAdd, rate, privKey, inputETH } = this.state;
    return (
      <Container style={styles.container}>
        <Header>
          <Body style={{ flex: 3 }}>
            <Title>Recharge BKC</Title>
          </Body>
        </Header>
        <Content padder>
          <Text style={{ ...styles.key, fontSize: 30 }}>Recharge BKC</Text>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>From Address: </Text>
            <Text style={styles.value}>{ethBalance.address}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>To ETH System Address:</Text>
            <Text style={styles.value}>{ethSysAdd}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Holder:</Text>
            <Text style={styles.value}>{userInfo.fullName}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>ETH Balance:</Text>
            <Text style={styles.value}>
              {parseInt(_.get(ethBalance, "balance"), 10) / Math.pow(10, 18) +
                " ETH"}
            </Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>BKC Wallet Balance:</Text>
            <Text style={styles.value}>{bkcBalance.balance} BKC</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Rate:</Text>
            <Text style={styles.value}>{rate} BKC/ETH</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <Input
              label="ethInput"
              placeholder="Input ETH"
              labelStyle={{ marginTop: 16 }}
              onChangeText={text =>
                this.setState({
                  inputETH: parseFloat(text.replace(/[^(\d+)\.(\d+)]/g, ""))
                })
              }
            />
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <Input
              label="privateKey"
              placeholder="PRIVATE KEY"
              labelStyle={{ marginTop: 16 }}
              value={privKey}
              secureTextEntry={true}
              onChangeText={text => this.setState({ privKey: text })}
            />
          </View>
          <Text style={{ ...styles.key, fontSize: 30 }}>After</Text>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Receive:</Text>
            <Text style={styles.value}>{parseFloat(rate) * inputETH} BKC</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Loss:</Text>
            <Text style={styles.value}>~{inputETH} ETH</Text>
          </View>
          <View
            style={{
              justifyContent: "flex-end",
              alignSelf: "flex-end",
              alignItems: "flex-end"
            }}
          >
            <Button
              onPress={() => this.onSubmitRechargeBKC()}
              transparent
              textStyle={{ color: "#87838B" }}
            >
              <IconMaterial size={75} color="#0051ff" name="cash" />
            </Button>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  bkcBalance: state.userReducer.bkcBalance,
  ethBalance: state.userReducer.ethBalance,
  userInfo: state.userReducer.userInfo
});

export default connect(mapStateToProps)(RechargeBKCContainer);
