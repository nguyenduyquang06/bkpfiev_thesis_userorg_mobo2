import * as React from "react";
import ContractDetails from "../../stories/screens/ContractDetails";
export interface Props {
	navigation: any,
}
export interface State {}
export default class ContractDetailsContainer extends React.Component<Props, State> {
	render() {
		return <ContractDetails navigation={this.props.navigation} />;
	}
}
