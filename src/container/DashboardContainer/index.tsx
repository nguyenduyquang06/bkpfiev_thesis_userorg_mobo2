import * as React from "react";
import { connect } from "react-redux";
import datas from "./data";
import { fetchList } from "./actions";
import Dashboard from "../../stories/screens/Dashboard";
export interface Props {
	navigation: any;
	fetchList: Function;
	data: Object;
}
export interface State {}
class DashboardContainer extends React.Component<Props, State> {
	componentDidMount() {
		this.props.fetchList(datas);
	}
	render() {
		return <Dashboard navigation={this.props.navigation} list={this.props.data} />;
	}
}

function bindAction(dispatch) {
	return {
		fetchList: url => dispatch(fetchList(url)),		
	};
}

const mapStateToProps = state => ({
	data: state.homeReducer.list,
	isLoading: state.homeReducer.isLoading,
});
export default connect(mapStateToProps, bindAction)(DashboardContainer);
