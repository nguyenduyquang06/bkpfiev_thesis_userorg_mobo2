import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Form,
  Item,
  Label,
  Input,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Toast,
  View,
  Picker,
  DatePicker
} from "native-base";
import styles from "./styles";
import { Col, Row, Grid } from "react-native-easy-grid";
import { Alert } from "react-native";
import {createCoffee} from "../../../service/coffeeBatch"
import { getCropID, getBlockID } from "../../../service/crop";
import _ from "lodash";
import moment from "moment";
export interface Props {
  navigation: any;
}

export interface State {
  cropID: string;
  beanSize: string;
  batchID: string;
  weight: string;
  listCrop: string[];
  blockID: string;
  listBlock: string[];
}

export interface State {}

const BEANSIZE = ["SMALL", "MEDIUM", "LARGE"];
class CoffeeBatchCreate extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      cropID: "",
      blockID: "",
      batchID: "",
      beanSize: BEANSIZE[0],
      weight: "",
      listCrop: [],
      listBlock: []
    };
  }

  async onSubmitCreateCoffee() {
    const { cropID, blockID, batchID, beanSize, weight } = this.state;
    // Works on both iOS and Android
    Alert.alert(
      "Confirm",
      "Are you sure you want submit ?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: async () => {
            if (cropID === "None" || blockID === "None" || batchID === ""|| weight === "") {
              return alert("Please fill out all information")  
            }
            console.log(cropID, blockID, batchID, beanSize, weight)
            const resp = await createCoffee(this.props.navigation,cropID, blockID, batchID, beanSize, weight)
            if (resp.success) {
              return alert("Create coffee batch successfully at transaction " + resp.transactionID)
            } else {
              return alert("Create coffee batch fail cause of " + resp.message)
            }
          }
        }
      ],
      { cancelable: true }
    );
    // const success = await createNewCrop(
    //   this.props.navigation,
    //   cropID,
    //   kindOfCoffee,
    //   origin,
    //   address
    // );
  }

  async componentDidMount() {
    const listCrop = await getCropID();
    const listBlock = await getBlockID();
    this.setState({ listCrop, listBlock });
    console.log(listBlock);
  }

  onChangeCropID(value) {
    console.log(value);
    this.setState({
      cropID: value
    });
  }

  onChangeBlockID(value) {
    console.log(value);
    this.setState({
      blockID: value
    });
  }

  onChangeBeansize(value) {
    console.log(value);
    this.setState({
      beanSize: value
    });
  }

  render() {
    const param = this.props.navigation.state.params;
    const { cropID, blockID, batchID, beanSize } = this.state;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Title>New coffee batch</Title>
          </Body>
          <Right />
        </Header>

        <Content padder>
          <Form>
            <Grid>
              <Row>
                <Col>
                  <Label>Crop ID</Label>
                  <Picker
                    mode="dropdown"
                    placeholder="Select crop"
                    iosIcon={<Icon name="arrow-down" />}
                    textStyle={{ color: "#5cb85c" }}
                    itemTextStyle={{ color: "#788ad2" }}
                    style={{
                      width: 150,
                      alignSelf: "center",
                      alignItems: "flex-end"
                    }}
                    selectedValue={cropID}
                    onValueChange={this.onChangeCropID.bind(this)}
                  >
                    <Picker.Item label={"None"} value={"None"} />
                    {_.map(this.state.listCrop, o => (
                      <Picker.Item label={o} key={o} value={o} />
                    ))}
                  </Picker>
                </Col>
                <Col>
                  <Label>Block ID</Label>
                  <Picker
                    mode="dropdown"
                    placeholder="Select block"
                    iosIcon={<Icon name="arrow-down" />}
                    textStyle={{ color: "#5cb85c" }}
                    itemTextStyle={{ color: "#788ad2" }}
                    style={{
                      width: 150,
                      alignSelf: "center",
                      alignItems: "flex-end"
                    }}
                    selectedValue={blockID}
                    onValueChange={this.onChangeBlockID.bind(this)}
                  >
                    <Picker.Item label={"None"} value={"None"} />
                    {_.map(this.state.listBlock, o => (
                      <Picker.Item label={o} key={o} value={o} />
                    ))}
                  </Picker>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Item floatingLabel>
                    <Label>Batch ID</Label>
                    <Input
                      onChangeText={value => this.setState({ batchID: value })}
                    />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Label>Bean size</Label>
                  <Picker
                    mode="dropdown"
                    placeholder="Bean size"
                    iosIcon={<Icon name="arrow-down" />}
                    textStyle={{ color: "#5cb85c" }}
                    itemTextStyle={{ color: "#788ad2" }}
                    style={{
                      width: 150,
                      alignSelf: "center",
                      alignItems: "flex-end"
                    }}
                    selectedValue={beanSize}
                    onValueChange={this.onChangeBeansize.bind(this)}
                  >
                    {_.map(BEANSIZE, o => (
                      <Picker.Item label={o} key={o} value={o} />
                    ))}
                  </Picker>
                </Col>
                <Col>
                  <Item floatingLabel last>
                    <Label>Weight</Label>
                    <Input
                      onChangeText={value => this.setState({ weight: value })}
                    />
                  </Item>
                </Col>
              </Row>
            </Grid>
            <Button
              onPress={() => {
                this.onSubmitCreateCoffee();
              }}
              style={{ alignSelf: "center" }}
              primary
            >
              <Text> Create </Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

export default CoffeeBatchCreate;
