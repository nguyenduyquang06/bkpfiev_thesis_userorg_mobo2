import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
  View,
  CardItem,
  Card,
  Label,
} from "native-base";
import { Col, Row, Grid } from "react-native-easy-grid";
import styles from "./styles";
import moment from "moment";
import _ from "lodash";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";

export interface Props {
  navigation: any;
}

class UpdateSensorHistory extends React.Component<Props> {
  state = {
    displayGraph: false,
  };
  render() {
    const { nodeHistory } = this.props.navigation.state.params;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>
          <Body style={{ flex: 4 }}>
            <Title>Sensor Update History</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Right>
            <IconMaterial.Button
              onPress={() => {
                this.setState({ displayGraph: !this.state.displayGraph });
              }}
              color="#0051ff"
              backgroundColor="#fff"
              size={30}
              name="eye"
            />
          </Right>
          {this.state.displayGraph ? (
            <Label>Display graph, not yet implemented</Label>
          ) : (
            _.map(nodeHistory, ({ node, timestamp }) => (
              <Card key={timestamp} style={{ flex: 0 }}>
                <CardItem>
                  <Left>
                    <Body>
                      <Grid>
                        <Row>
                          <Col>
                            <IconMaterial
                              color="#FF0000"
                              size={50}
                              name="oil-temperature"
                            />
                            <Label>{`${node.sensor.temperature} *C`}</Label>
                          </Col>
                          <Col>
                            <IconMaterial
                              color="#7EC0EE"
                              size={50}
                              name="water"
                            />
                            <Label>{`${node.sensor.humidity}%`}</Label>
                          </Col>
                          <Col>
                            <IconMaterial
                              color="#215732"
                              size={50}
                              name="update"
                            />
                            <Label>{`${moment
                              .unix(timestamp)
                              .format("HH:mm:ss DD/MM/YY")}`}</Label>
                          </Col>
                        </Row>
                      </Grid>
                    </Body>
                  </Left>
                </CardItem>
              </Card>
            ))
          )}
        </Content>
      </Container>
    );
  }
}

export default UpdateSensorHistory;
