import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Form,
  Item,
  Label,
  Input,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Toast,
  View,
  DatePicker,
} from "native-base";
import styles from "./styles";
import { Col, Row, Grid } from "react-native-easy-grid";
import { createNewCrop } from "../../../service/crop";
import moment from "moment";
export interface Props {
  navigation: any;
}

export interface State {
  cropID: string;
  kindOfCoffee: string;
  origin: string;
  address: string;
}

export interface State {}
class CropCreate extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      cropID: "",
      kindOfCoffee: "",
      origin: "",
      address: "",
    };
  }

  async onSubmitNewCrop() {
    const { cropID, kindOfCoffee, origin, address } = this.state;
    // console.log(cropID, kindOfCoffee, origin, fromDate, address);
    const success = await createNewCrop(
      this.props.navigation,
      cropID,
      kindOfCoffee,
      origin,
      address,
    );
    if (success) {
      Toast.show({
        text: `Create ${cropID} successfully`,
        duration: 2000,
        type: "success",
        textStyle: { textAlign: "center" },
      });
    }
  }
  render() {
    const param = this.props.navigation.state.params;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Title>New Crop</Title>
          </Body>
          <Right />
        </Header>

        <Content padder>
          <Form>
            <Grid>
              <Col>
                <Item floatingLabel>
                  <Label>Crop ID</Label>
                  <Input
                    onChangeText={(value) => this.setState({ cropID: value })}
                  />
                </Item>
              </Col>
              <Col>
                <Item floatingLabel last>
                  <Label>Kind Of Coffee</Label>
                  <Input
                    onChangeText={(value) =>
                      this.setState({ kindOfCoffee: value })
                    }
                  />
                </Item>
                {/* <Item stackedLabel>
                  <Label>Made from date</Label>
                  <View >
                    <DatePicker
                      locale={"en"}
                      timeZoneOffsetInMinutes={undefined}
                      modalTransparent={false}
                      animationType={"fade"}
                      androidMode={"default"}
                      placeHolderText="Date"
                      textStyle={{ fontSize: 15, color: "black" }}
                      placeHolderTextStyle={{ fontSize: 15, color: "#d3d3d3" }}
                      onDateChange={(newDate) => {
                        // console.log(moment(newDate).format())
                        this.setState({ chosenDate: newDate });
                      }}
                      disabled={false}
                    />
                  </View>
                </Item> */}
              </Col>
            </Grid>
            <Item floatingLabel last>
              <Label>Origin</Label>
              <Input onChangeText={(value) => this.setState({ origin: value })} />
            </Item>
            <Item floatingLabel last>
              <Label>Address</Label>
              <Input
                onChangeText={(value) => this.setState({ address: value })}
              />
            </Item>
            <Button
              onPress={() => {
                this.onSubmitNewCrop();
              }}
              style={{ alignSelf: "center" }}
              primary
            >
              <Text> New Crop </Text>
            </Button>
          </Form>
        </Content>
      </Container>
    );
  }
}

export default CropCreate;
