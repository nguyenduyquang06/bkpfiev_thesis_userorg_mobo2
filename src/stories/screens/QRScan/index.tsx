import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
  View
} from "native-base";
import { connect } from "react-redux";
import * as userAction from "../../../container/UserContainer/actions";
import { bindActionCreators } from "redux";
import styles from "./styles";
import { BarCodeScanner, Permissions } from "expo";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";

export interface Props {
  navigation: any;
}
export interface State {
  hasCameraPermission: boolean;
  openCamera: boolean;
}
class QRScan extends React.Component<Props, State> {
  state = {
    hasCameraPermission: null,
    openCamera: false
  };

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === "granted" });
    // const { actions } = this.props;
    // await actions.getUserInfo();
    // const { userInfo } = this.props;    
    // await actions.getBKCBalance(userInfo.userID);
    // await actions.getETHBalance(userInfo.ethAddress);
  }

  handleBarCodeScanned = ({ type, data }) => {
    this.setState({
      openCamera: false
    });
    try {
      const qrObject = JSON.parse(data);
      if (typeof qrObject.uniqueProductID === "undefined") {
        alert("Cannot find uniqueProductID in this QR code");
      } else {
        alert("Valid QR code, querying Blockchain system . . .");
        this.props.navigation.navigate("ProductInformation", {
          PRODUCT: qrObject
        });
      }
    } catch (err) {
      alert(err);
    }
  };

  render() {
    const param = this.props.navigation.state.params;
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <Text>Requesting for camera permission</Text>;
    }
    if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    }

    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.openDrawer()}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Title>Home - QR Scan</Title>
          </Body>
        </Header>
        <Content padder>
          <Text>
            Get product information and track back by capture product QR code
          </Text>
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              marginTop: 150
            }}
          >
            {this.state.openCamera ? (
              <View>
                <BarCodeScanner
                  onBarCodeScanned={this.handleBarCodeScanned}
                  style={{ maxHeight: 250, height: 250, width: 250 }}
                />
                <Button
                  onPress={() => {
                    this.setState({
                      openCamera: false
                    });
                  }}
                  style={{ alignSelf: "center", marginTop: 50 }}
                  primary
                >
                  <Text> Cancel </Text>
                </Button>
              </View>
            ) : (
              <View>
                <Button
                  onPress={() => {
                    this.setState({
                      openCamera: true
                    });
                  }}
                  transparent
                  style={{ height: 300 }}
                  textStyle={{ color: "#87838B" }}
                >
                  <IconMaterial size={200} color="#0051ff" name="qrcode-scan" />
                </Button>
              </View>
            )}
          </View>
        </Content>
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ ...userAction }, dispatch)
  };
};

const mapStateToProps = state => ({
  userInfo: state.userReducer.userInfo,
  bkcBalance: state.userReducer.bkcBalance
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QRScan);
