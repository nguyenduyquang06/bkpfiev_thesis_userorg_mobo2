import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Form,
  Item,
  Label,
  Input,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Toast,
  View,
  DatePicker,
} from "native-base";
import styles from "./styles";
import { Col, Row, Grid } from "react-native-easy-grid";
import { createNode } from "../../../service/smartFarm";
import moment from "moment";
import { connect } from "react-redux";
export interface Props {
  navigation: any;
}

export interface State {
  nodeID: string;
}

export interface State {}
class NodeCreate extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      nodeID: "",
    };
  }

  async onSubmitNewNode() {
    const { selectedCrop, selectedSmartFarm } = this.props;
    const { nodeID } = this.state;    
    const success = await createNode(
      this.props.navigation,
      selectedCrop.id,
      selectedSmartFarm.blockID,
      nodeID,
    );

    if (success) {
      Toast.show({
        text: `Create node ${nodeID} successfully`,
        duration: 2000,
        type: "success",
        textStyle: { textAlign: "center" },
      });
    }
  }
  render() {
    const { selectedCrop, selectedSmartFarm } = this.props;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Title>New SmartFarm</Title>
          </Body>
          <Right />
        </Header>

        <Content padder>
          <Form>
            <Grid>
              <Row>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Crop ID</Label>
                    <Input value={selectedCrop.id} disabled />
                  </Item>
                </Col>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Block ID</Label>
                    <Input value={selectedSmartFarm.blockID} disabled />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Node ID</Label>
                    <Input
                      value={this.state.nodeID}
                      onChangeText={(e) => this.setState({ nodeID: e })}
                    />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Button
                    onPress={() => {
                      this.onSubmitNewNode();
                    }}
                    style={{ alignSelf: "center" }}
                    primary
                  >
                    <Text> New Node </Text>
                  </Button>
                </Col>
                <Col>
                  <Button
                    backgroundColor="#215732"
                    onPress={() => {
                      this.props.navigation.goBack();
                    }}
                    style={{ alignSelf: "center" }}
                    primary
                  >
                    <Text> Cancel </Text>
                  </Button>
                </Col>
              </Row>
            </Grid>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  selectedCrop: state.cropsReducer.selectedCrop,
  selectedSmartFarm: state.cropsReducer.selectedSmartFarm,
});

export default connect(mapStateToProps)(NodeCreate);
