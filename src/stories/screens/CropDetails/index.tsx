import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
  View,
} from "native-base";
import { connect } from "react-redux";
import styles from "./styles";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
export interface Props {
  navigation: any;
  selectedCrop: object;
}
export interface State {}
class CropDetails extends React.Component<Props, State> {
  render() {
    const param = this.props.navigation.state.params;
    const { selectedCrop } = this.props;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>

          <Body style={{ flex: 4 }}>
            <Title>Crop Information</Title>
          </Body>

          <Right />
        </Header>

        <Content padder>
          <Text style={{ ...styles.key, fontSize: 30 }}>
            Details
          </Text>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Product:</Text>
            <Text style={styles.value}>Coffee Bean</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="home-automation" />
            <Text style={styles.key}>Crop ID:</Text>
            <Text style={styles.value}>{selectedCrop.id}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="domain" />
            <Text style={styles.key}>Grower MSP ID:</Text>
            <Text style={styles.value}>{selectedCrop.growerMSPID}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="account-circle" />
            <Text style={styles.key}>Grower ID:</Text>
            <Text style={styles.value}>{selectedCrop.growerID}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="coffee" />
            <Text style={styles.key}>Kind of Coffee</Text>
            <Text style={styles.value}>{selectedCrop.kindOfCoffee}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Origin:</Text>
            <Text style={styles.value}>{selectedCrop.origin}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="map-marker" />
            <Text style={styles.key}>Address:</Text>
            <Text style={styles.value}>{selectedCrop.address}</Text>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  selectedCrop: state.cropsReducer.selectedCrop,
});

export default connect(mapStateToProps)(CropDetails);
