import { StyleSheet } from "react-native";

const styles: any = StyleSheet.create({
  container: {
    backgroundColor: "#FBFAFA"
  },
  key: {
    fontWeight: "bold",
    fontSize: 17,
    paddingLeft: 10,
    color: "#215732"
  },
  value: {
    fontSize: 17,
    paddingLeft: 10,
    color: "#215732"
  },
  list: {
    flex: 1,
    marginTop: 20
  },
  title: {
    fontSize: 16,
    fontWeight: "bold"
  },
  descriptionContainer: {
    flexDirection: "row",
    paddingRight: 50
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 25
  },
  textDescription: {
    marginLeft: 10,
    color: "gray"
  }
});
export default styles;
