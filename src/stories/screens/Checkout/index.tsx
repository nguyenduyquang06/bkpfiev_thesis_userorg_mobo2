import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Right,
  Body,
  List,
  ListItem,
  View
} from "native-base";
import { connect } from "react-redux";
import { Input } from "react-native-elements";
import { Alert } from "react-native";
import styles from "./styles";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { payProduct } from "../../../service/product";
// import { PRODUCT } from "./mock.js";
import _ from "lodash";
import moment from "moment";

export interface Props {
  navigation: any;
}

export interface State {}
class Checkout extends React.Component<Props, State> {
  state = {
    walletPW: ""
  };

  constructor(props) {
    super(props);
  }

  async onSubmitPayProduct() {
    const { walletPW } = this.state;
    if (walletPW === "") {
      alert("Wallet password empty");
      return;
    }
    Alert.alert(
      "Confirm",
      "Are you sure buy this product ?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "OK",
          onPress: async () => {
            alert("Paying. . .");
            const param = this.props.navigation.state.params;
            const {
              PRODUCT: { uniqueProductID },
              onSuccessPay
            } = param;
            const params = {
              fromPassword: walletPW,
              uniqueProductID
            };
            const resp = await payProduct(params);
            if (resp.success) {
              alert("Pay successfully at transaction ID " + resp.transactionID);
              onSuccessPay(uniqueProductID);
              this.props.navigation.goBack();
              return;
            }
            alert("Pay fail due to " + resp.message);
          }
        }
      ],
      { cancelable: true }
    );
  }

  render() {
    const {} = this.state;
    const { bkcBalance, userInfo } = this.props;
    const param = this.props.navigation.state.params;
    const {
      PRODUCT: { uniqueProductID },
      price,
      quantity
    } = param;
    return (
      <Container style={styles.container}>
        <Header>
          <Body style={{ flex: 3 }}>
            <Title>Checkout</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Text style={{ ...styles.key, fontSize: 30 }}>Order summary</Text>
          <List>
            <ListItem>
              <Body>
                <Text>{`${uniqueProductID.productLineID} - ${
                  uniqueProductID.batchProductID
                }.${uniqueProductID.productID} x ${quantity}`}</Text>
                <Text note>Quantity:{quantity}</Text>
              </Body>
              <Right>
                <Text>{price * quantity} BKC</Text>
              </Right>
            </ListItem>
          </List>
          <Text style={{ ...styles.key, fontSize: 30 }}>Payment Details</Text>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Payment: </Text>
            <Text style={styles.value}>BKC cryptocurrency</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Total price:</Text>
            <Text style={styles.value}>{price * quantity} BKC</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Holder:</Text>
            <Text style={styles.value}>{userInfo.fullName}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Wallet Balance:</Text>
            <Text style={styles.value}>{bkcBalance.balance}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Address:</Text>
            <Text style={styles.value}>{bkcBalance.address}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <Input
              label="Wallet password"
              labelStyle={{ marginTop: 16 }}
              secureTextEntry={true}
              onChangeText={text => this.setState({ walletPW: text })}
            />
          </View>
          <View
            style={{
              justifyContent: "flex-end",
              alignSelf: "flex-end",
              alignItems: "flex-end"
            }}
          >
            <Button
              onPress={() => {
                if (_.isEmpty(bkcBalance)) {
                  alert("Wallet not found, you need create it first");
                  return;
                }
                if (bkcBalance.balance < price) {
                  alert("You don't have enough balance to buy this product");
                  return;
                }
                this.onSubmitPayProduct();
              }}
              transparent
              textStyle={{ color: "#87838B" }}
            >
              <IconMaterial size={75} color="#0051ff" name="cash" />
            </Button>
            <Text
              style={{
                ...styles.key,
                color: "#0051ff",
                fontSize: 25,
                marginRight: 20
              }}
            >
              Buy
            </Text>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  bkcBalance: state.userReducer.bkcBalance,
  userInfo: state.userReducer.userInfo
});

export default connect(mapStateToProps)(Checkout);
