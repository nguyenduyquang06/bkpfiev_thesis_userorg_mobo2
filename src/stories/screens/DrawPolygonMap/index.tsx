import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Form,
  Item,
  Label,
  Input,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Toast,
  View,
  DatePicker,
} from "native-base";
import styles from "./styles";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { MapView } from "expo";
import { Col, Row, Grid } from "react-native-easy-grid";
import _ from "lodash";
export interface Props {
  navigation: any;
}

export interface State {
  testList: Array<{
    latitude: number;
    longitude: number;
  }>;
  firstPoint: {
    latitude: number;
    longitude: number;
  };
  mapType: boolean;
  latitude: number;
  longitude: number;
  latitudeDelta: number;
  longitudeDelta: number;
}

export interface State {}
class DrawPolygonMap extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      testList: [],
      mapType: false,
      latitude: 10.742483,
      longitude: 106.668338,
      latitudeDelta: 0.01,
      longitudeDelta: 0.01,
      firstPoint: {
        latitude: 10.742483,
        longitude: 106.668338,
      },
    };
  }

  goBack() {
    const { navigation } = this.props;
    const { testList } = this.state;
    if (testList.length !== 4) {
      Toast.show({
        text: `Lưu ý chọn 4 điểm`,
        duration: 2000,
        type: "warning",
        textStyle: { textAlign: "center" },
      });
    } else {
      navigation.goBack();
      navigation.state.params.onRegionSelected({ region: testList });
    }
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Title>Location</Title>
          </Body>
          <Right />
        </Header>
        <Content padder>
          <Text>
            {/* Locate your smart farm location by pressing 4 points on the map */}
            Định vị trang trại của bạn bằng cách nhấn chạm 4 điểm trên bản đồ
          </Text>
          <Text>Nếu chọn không đúng có thể clear để chọn lại</Text>
          <Row>
            <Label style={styles.key}> Điểm centroid: </Label>
          </Row>
          <Row>
            <Col>
              <Item floatingLabel>
                <Label style={styles.value}>Latitude</Label>
                <Input
                  value={
                    this.state.latitude ? this.state.latitude.toString() : ""
                  }
                  onChangeText={(text) => {
                    this.setState({
                      latitude: parseFloat(text.replace(/[^(\d+)\.(\d+)]/g, "")),
                    });
                  }}
                  keyboardType="numeric"
                />
              </Item>
            </Col>
            <Col>
              <Item floatingLabel>
                <Label style={styles.value}>Longitude</Label>
                <Input
                  value={
                    this.state.longitude ? this.state.longitude.toString() : ""
                  }
                  onChangeText={(text) => {
                    this.setState({
                      longitude: parseFloat(
                        text.replace(/[^(\d+)\.(\d+)]/g, ""),
                      ),
                    });
                  }}
                  keyboardType="numeric"
                />
              </Item>
            </Col>
          </Row>
          <Row>
            <Col>
              <Button
                onPress={() => {
                  if (this.state.longitude && this.state.latitude) {
                    this.setState({
                      firstPoint: {
                        latitude: this.state.latitude,
                        longitude: this.state.longitude,
                      },
                    });
                  }
                }}
                style={{ alignSelf: "center" }}
                primary
              >
                <Text> Tìm kiếm </Text>
              </Button>
            </Col>
            <Col>
              <Button
                backgroundColor="#215732"
                onPress={() => {
                  this.setState({
                    testList: [],
                  });
                }}
                style={{ alignSelf: "center" }}
                primary
              >
                <Text> Clear </Text>
              </Button>
            </Col>
          </Row>
          <MapView
            style={{ alignSelf: "stretch", height: 300 }}
            mapType={this.state.mapType ? "satellite" : "standard"}
            region={{
              latitude: this.state.firstPoint.latitude,
              longitude: this.state.firstPoint.longitude,
              latitudeDelta: this.state.latitudeDelta,
              longitudeDelta: this.state.longitudeDelta,
            }}
            onPress={(e) =>
              this.setState({
                testList: [...this.state.testList, e.nativeEvent.coordinate],
              })
            }
            onRegionChangeComplete={(r) => {
              this.setState({
                longitudeDelta: r.longitudeDelta,
                latitudeDelta: r.latitudeDelta,
                firstPoint: {
                  longitude: r.longitude,
                  latitude: r.latitude,
                },
              });
            }}
          >
            {this.state.testList.length !== 0 ? (
              <MapView.Polygon
                coordinates={this.state.testList}
                strokeWidth={2}
                fillOpacity={1}
                draggable
                editable
                strokeColor="#215732"
                fillColor="#368e52"
              />
            ) : null}
            {_.map(this.state.testList, ({ latitude, longitude }, key) => (
              <MapView.Marker
                coordinate={{
                  latitude,
                  longitude,
                }}
                key={longitude}
                title={`Point ${key.toString()}`}
                description={`Latitude:${latitude.toPrecision(
                  4,
                )}, Longitude:${longitude.toPrecision(4)}`}
              />
            ))}
          </MapView>
          <Row>
            <Button
              backgroundColor="#215732"
              onPress={() => {
                this.goBack();
              }}
              style={{ alignSelf: "center" }}
              primary
            >
              <Text> Save and Goback </Text>
            </Button>
            <Right>
              <IconMaterial.Button
                onPress={() => {
                  this.setState({ mapType: !this.state.mapType });
                }}
                color="#0051ff"
                backgroundColor="transparent"
                size={30}
                name="eye"
              />
            </Right>
          </Row>
        </Content>
      </Container>
    );
  }
}

export default DrawPolygonMap;
