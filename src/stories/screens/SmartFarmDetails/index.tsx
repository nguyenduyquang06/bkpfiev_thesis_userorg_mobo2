import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Icon,
  Left,
  Right,
  Body,
  View,
  Toast,
  Picker,
  DatePicker,
} from "native-base";
import { Stage } from "./constant";
import { connect } from "react-redux";
import styles from "./styles";
import moment from "moment";
import { MapView } from "expo";
import _ from "lodash";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { updateSmartFarmToDate, updateStage } from "../../../service/smartFarm";
export interface Props {
  navigation: any;
}
export interface State {
  editStageMode: boolean;
  stage: string;
  editToDateMode: boolean;
  toDate: string;
}

class SmartFarmDetails extends React.Component<Props, State> {
  state = {
    mapType: false,
    editStageMode: false,
    editToDateMode: false,
    stage: "",
    toDate: "",
    testList: [],
  };

  onValueChange(value) {
    this.setState({
      stage: value,
    });
  }
  async onSubmitStage() {
    const {
      selectedSmartFarm: { blockID },
      selectedCrop: { id },
    } = this.props;
    const { stage } = this.state;
    const success = await updateStage(
      this.props.navigation,
      id,
      blockID,
      stage,
    );
    if (success) {
      Toast.show({
        text: `Update stage ${stage} successfully`,
        duration: 2000,
        type: "success",
        textStyle: { textAlign: "center" },
      });
      this.setState({
        editStageMode: false,
      });
    }
  }
  async onSubmitSFToDate() {
    const {
      selectedSmartFarm: { blockID },
      selectedCrop: { id },
    } = this.props;
    const { toDate } = this.state;
    const success = await updateSmartFarmToDate(
      this.props.navigation,
      id,
      blockID,
      moment(toDate).format(),
    );
    if (success) {
      Toast.show({
        text: `Update toDate ${toDate} successfully`,
        duration: 2000,
        type: "success",
        textStyle: { textAlign: "center" },
      });
      this.setState({
        editToDateMode: false,
      });
    }
  }

  render() {
    const { selectedSmartFarm } = this.props;
    const param = this.props.navigation.state.params;
    const { latLongList, lat, long } = param;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>

          <Body style={{ flex: 4 }}>
            <Title>Smart Farm Information</Title>
          </Body>

          <Right />
        </Header>
        <Content padder>
          <View
            style={{
              flexDirection: "row",
              flexWrap: "wrap",
            }}
          >
            <Text style={{ fontWeight: "bold", fontSize: 30 }}>Details</Text>
            <Right>
              {selectedSmartFarm.stage !== Stage.CROPPED ? (
                <IconMaterial.Button
                  backgroundColor="transparent"
                  color="#215732"
                  size={30}
                  name="leaf"
                  onPress={() =>
                    this.setState({
                      editStageMode: !this.state.editStageMode,
                      stage: "",
                    })
                  }
                />
              ) : null}

              {!selectedSmartFarm.ToDate ? (
                <IconMaterial.Button
                  backgroundColor="transparent"
                  color="#215732"
                  size={30}
                  name="calendar-check"
                  onPress={() =>
                    this.setState({
                      editToDateMode: !this.state.editToDateMode,
                      toDate: "",
                    })
                  }
                />
              ) : null}
            </Right>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Block ID:</Text>
            <Text style={styles.value}>{selectedSmartFarm.blockID}</Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="leaf" />
            <Text style={styles.key}>Stage:</Text>
            <Picker
              mode="dropdown"
              placeholder="Select crop"
              iosIcon={<Icon name="arrow-down" />}
              textStyle={{ color: "#5cb85c" }}
              itemTextStyle={{ color: "#788ad2" }}
              enabled={this.state.editStageMode}
              style={{
                width: 140,
                marginTop: -10,
              }}
              selectedValue={
                this.state.stage ? this.state.stage : selectedSmartFarm.stage
              }
              onValueChange={this.onValueChange.bind(this)}
            >
              {_.map(Stage, (value, key) => (
                <Picker.Item label={value} key={value} value={value} />
              ))}
            </Picker>
            {this.state.editStageMode &&
            this.state.stage !== selectedSmartFarm.stage ? (
              <IconMaterial.Button
                backgroundColor="transparent"
                color="#215732"
                size={30}
                name="circle-edit-outline"
                style={{
                  marginTop: -10,
                }}
                onPress={() => this.onSubmitStage()}
              />
            ) : null}
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="format-list-bulleted" />
            <Text style={styles.key}>List of Node ID:</Text>
            <Text style={styles.value}>
              {selectedSmartFarm.NodeIDList.toString()}
            </Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="calendar-edit" />
            <Text style={styles.key}>Made from:</Text>
            <Text style={styles.value}>
              {selectedSmartFarm.FromDate
                ? moment(selectedSmartFarm.FromDate).format("DD/MM/YYYY")
                : "Not yet updated"}
            </Text>
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="calendar-check" />
            <Text style={styles.key}>Made to:</Text>
            {this.state.editToDateMode ? (
              <View style={{ marginTop: -8, width: 120 }}>
                <DatePicker
                  locale={"en"}
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType={"fade"}
                  androidMode={"default"}
                  placeHolderText="Date"
                  textStyle={{ fontSize: 15, color: "black" }}
                  placeHolderTextStyle={{
                    fontSize: 15,
                    color: "#d3d3d3",
                  }}
                  onDateChange={(newDate) => {
                    this.setState({ toDate: newDate });
                  }}
                  disabled={false}
                />
              </View>
            ) : (
              <Text style={styles.value}>
                {selectedSmartFarm.ToDate
                  ? moment(selectedSmartFarm.ToDate).format("DD/MM/YYYY")
                  : "Not yet updated"}
              </Text>
            )}
            {this.state.editToDateMode ? (
              <IconMaterial.Button
                backgroundColor="transparent"
                color="#215732"
                size={30}
                name="circle-edit-outline"
                style={{
                  marginTop: -10,
                }}
                onPress={() => this.onSubmitSFToDate()}
              />
            ) : null}
          </View>
          <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
            <IconMaterial size={30} name="map-marker-radius" />
            <Text style={styles.key}>Position:</Text>
          </View>
          <MapView
            style={{ alignSelf: "stretch", height: 300 }}
            mapType={this.state.mapType ? "satellite" : "standard"}
            initialRegion={{
              latitude: lat,
              longitude: long,
              latitudeDelta: 0.00001,
              longitudeDelta: 0.00001,
            }}
            onPress={(e) =>
              this.setState({
                testList: [...this.state.testList, e.nativeEvent.coordinate],
              })
            }
          >
            <MapView.Polygon
              coordinates={latLongList}
              strokeWidth={2}
              fillOpacity={1}
              draggable
              editable
              strokeColor="#215732"
              fillColor="#368e52"
            />
            {this.state.testList.length !== 0 ? (
              <MapView.Polygon
                coordinates={this.state.testList}
                strokeWidth={2}
                fillOpacity={1}
                draggable
                editable
                strokeColor="#215732"
                fillColor="#368e52"
              />
            ) : null}

            {/* {_.map(latLongList, ({latitude,longitude}) => (
                      <MapView.Marker
                        coordinate={{
                          latitude,
                          longitude
                        }}
                        key={longitude}
                        title={o.blockID}
                        description={`Smart Farm ID: ${o.blockID}`}
                      />
                    ))} */}
          </MapView>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  selectedSmartFarm: state.cropsReducer.selectedSmartFarm,
  selectedCrop: state.cropsReducer.selectedCrop,
});

export default connect(mapStateToProps)(SmartFarmDetails);
