import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Right,
  Body,
  Tab,
  Tabs,
  View,
  List,
  Left,
  ListItem,
  Picker,
  Icon
} from "native-base";
import { connect } from "react-redux";
import { Input } from "react-native-elements";
import { AirbnbRating } from "react-native-ratings";
import styles from "./styles";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import { getReviewUsedProduct } from "../../../service/product";
import { getHistoryCoffeeBatch } from "../../../service/coffeeBatch";
import _ from "lodash";
import moment from "moment";
import Timeline from "react-native-timeline-listview";

export interface Props {
  navigation: any;
}

export interface State {}
class ReviewHistory extends React.Component<Props, State> {
  state = { productReviews: [] };

  constructor(props) {
    super(props);    
  }

  async componentDidMount() {
    const {
      userInfo: { userID }
    } = this.props;
    const productReviews = await getReviewUsedProduct(userID);
    this.setState({
      productReviews,
    });
  }
  render() {
    const { productReviews } = this.state;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.openDrawer()}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Title>Review History</Title>
          </Body>
        </Header>
        <Content padder>
          <Text style={{ ...styles.key, fontSize: 30 }}>Reviews</Text>
          <List>
            {_.map(productReviews, o => (
              <ListItem key={o.reviewTime} avatar>
                <Body>
                  <Text>{`${_.get(
                    o.uniqueProductID,
                    "productLineID"
                  )} - ${_.get(o.uniqueProductID, "batchProductID")} - productID: ${_.get(
                    o.uniqueProductID,
                    "productID"
                  )}`}</Text>
                  <Text note>{o.comment}</Text>
                </Body>
                <Right>
                  <Text note>
                    {moment.unix(o.reviewTime).format("MM-DD-YYYY")}
                  </Text>
                  <Text>{o.score} stars</Text>
                </Right>
              </ListItem>
            ))}
          </List>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  userInfo: state.userReducer.userInfo,
});

export default connect(mapStateToProps)(ReviewHistory);
