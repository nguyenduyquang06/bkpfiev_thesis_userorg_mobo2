import * as React from "react";
import { NavigationActions } from "react-navigation";
import { Image } from "react-native";
import {
  Content,
  Text,
  List,
  ListItem,
  Icon,
  Container,
  Left,
  Right,
  Badge
} from "native-base";
import styles from "./style";
const datas = [
  {
    route: "Home",
    caption: "Home",
    icon: "phone-portrait",
    bg: "#C5F442",
  },
  // {
  //   route: "Home",
  //   caption: "Scan History",
  //   icon: "phone-portrait",
  //   bg: "#C5F442",
  // },
  {
    route: "ReviewHistory",
    caption: "Review History",
    icon: "phone-portrait",
    bg: "#C5F442",
  },
  {
    route: "User",
    caption: "User",
    icon: "phone-portrait",
    bg: "#C5F442",
  },
  {
    route: "Login",
    caption: "Login",
    icon: "phone-portrait",
    bg: "#C5F442"
  }
];
const drawerCover = require("../../../../assets/cover.png");
const drawerImage = require("../../../../assets/bk_60nam.png");
export interface Props {
  navigation: any;
}
export interface State {}
const resetAction = NavigationActions.navigate({ routeName: "Login" });
export default class Sidebar extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      shadowOffsetWidth: 1,
      shadowRadius: 4
    };
  }

  render() {
    return (
      <Container>
        <Content
          bounces={false}
          style={{ flex: 1, backgroundColor: "#fff", top: -1 }}
        >
          <Image source={drawerCover} style={styles.drawerCover} />
          <Image square style={styles.drawerImage} source={drawerImage} />

          <List
            dataArray={datas}
            renderRow={data =>
              <ListItem
                button
                noBorder
                onPress={() => this.props.navigation.navigate(data.route)}
              >
                <Left>
                  <Icon
                    active
                    name={data.icon}
                    style={{ color: "#777", fontSize: 26, width: 30 }}
                  />
                  <Text style={styles.text}>
                    {data.caption}
                  </Text>
                </Left>
                {data.types &&
                  <Right style={{ flex: 1 }}>
                    <Badge
                      style={{
                        borderRadius: 3,
                        height: 25,
                        width: 72,
                        backgroundColor: data.bg
                      }}
                    >
                      <Text
                        style={styles.badgeText}
                      >{`${data.types} Types`}</Text>
                    </Badge>
                  </Right>}
              </ListItem>}
          />
        </Content>
      </Container>


      // <Container>
      //   <Content>
      //     <List
      //       style={{ marginTop: 40 }}
      //       dataArray={routes}
      //       renderRow={data => {
      //         return (
      //           <ListItem
      //             button
      //             onPress={() => {
      //               data.route === "Login"
      //                 ? this.props.navigation.reset([resetAction], 0)
      //                 : this.props.navigation.navigate(data.route);
      //             }}
      //           >
      //             <Text>{data.caption}</Text>
      //           </ListItem>
      //         );
      //       }}
      //     />
      //   </Content>
      // </Container>
    );
  }
}
