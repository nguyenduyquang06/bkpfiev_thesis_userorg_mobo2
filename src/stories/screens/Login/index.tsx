import * as React from "react";
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  Dimensions,
  LayoutAnimation,
  UIManager,
  KeyboardAvoidingView
} from "react-native";
import { connect } from "react-redux";
import { Facebook } from "expo";
import { Font } from "expo";
import { Input, Button } from "react-native-elements";
import { Toast } from "native-base";
import { login, loginViaFB, registerAccount } from "../../../service/auth";
import Icon from "react-native-vector-icons/FontAwesome";
import SimpleIcon from "react-native-vector-icons/SimpleLineIcons";
import * as userAction from "../../../container/UserContainer/actions";
import { bindActionCreators } from "redux";

const SCREEN_WIDTH = Dimensions.get("window").width;
const SCREEN_HEIGHT = Dimensions.get("window").height;

const BG_IMAGE = require("../../../../assets/images/bg_screen4.jpg");
// Enable LayoutAnimation on Android
UIManager.setLayoutAnimationEnabledExperimental &&
  UIManager.setLayoutAnimationEnabledExperimental(true);

const TabSelector = ({ selected }) => {
  return (
    <View style={styles.selectorContainer}>
      <View style={selected && styles.selected} />
    </View>
  );
};
//import styles from "./styles";
export interface Props {
  navigation: any;
}
export interface State {
  email: string;
  password: string;
  fontLoaded: boolean;
  selectedCategory: number;
  isLoading: boolean;
  isEmailValid: boolean;
  isUsernameValid: boolean;
  isPasswordValid: boolean;
  isConfirmationValid: boolean;
  passwordConfirmation: string;
  ethAddress: string;
}

class Login extends React.Component<Props, State> {
  constructor(props) {
    super(props);

    this.state = {
      email: "nguyenduyquang06@gmail.com",
      password: "",
      fontLoaded: false,
      selectedCategory: 0,
      passwordConfirmation: "",
      isLoading: false,
      isEmailValid: true,
      isUsernameValid: true,
      isPasswordValid: true,
      isConfirmationValid: true,
      ethAddress: ""
    };
    this.selectCategory = this.selectCategory.bind(this);
    this.login = this.login.bind(this);
    this.signUpViaFB = this.signUpViaFB.bind(this);
    this.logInViaFB = this.logInViaFB.bind(this);
  }

  selectCategory(selectedCategory) {
    LayoutAnimation.easeInEaseOut();
    this.setState({
      selectedCategory,
      isLoading: false
    });
  }
  async componentDidMount() {
    await Font.loadAsync({
      georgia: require("../../../../assets/fonts/Georgia.ttf"),
      regular: require("../../../../assets/fonts/Montserrat-Regular.ttf"),
      light: require("../../../../assets/fonts/Montserrat-Light.ttf"),
      bold: require("../../../../assets/fonts/Montserrat-Bold.ttf")
    });
    this.props.actions.resetUser();
    this.setState({ fontLoaded: true });
  }

  validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  validateUsername(username) {
    return username.length >= 3;
  }

  async logInViaFB() {
    try {
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions
      } = await Facebook.logInWithReadPermissionsAsync("331134924419883", {
        permissions: ["public_profile", "email"]
      });
      Toast.show({
        text: "Waiting response from server",
        duration: 1000,
        type: "warning",
        textStyle: { textAlign: "center" }
      });
      if (type === "success") {
        // Get the user's name using Facebook's Graph API
        const response = await fetch(
          `https://graph.facebook.com/me?fields=id,name,email&access_token=${token}`
        );
        const resp = await loginViaFB(token);
        if (resp.success) {
          Toast.show({
            text: "Login successfully!",
            duration: 2000,
            type: "success",
            textStyle: { textAlign: "center" }
          });
          const { actions } = this.props;
          await actions.getUserInfo();
          const { userInfo } = this.props;
          await actions.getBKCBalance(userInfo.userID);
          await actions.getETHBalance(userInfo.ethAddress);
          this.props.navigation.navigate("Drawer");
        } else {
          Toast.show({
            text: `Login fail! ${resp.message}`,
            duration: 2000,
            type: "danger",
            textStyle: { textAlign: "center" }
          });
        }
      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      alert(`Facebook Login Error: ${message}`);
    }
  }

  async login() {
    const { email, password } = this.state;
    this.setState({ isLoading: true });
    // Simulate an API call
    // LayoutAnimation.easeInEaseOut();
    const isUsernameValid =
      this.validateUsername(email) || this.usernameInput.shake();
    const isPasswordValid = password.length >= 4 || this.passwordInput.shake();
    this.setState({
      isLoading: false,
      isUsernameValid,
      isPasswordValid
    });
    Toast.show({
      text: "Waiting response from server",
      duration: 1000,
      type: "warning",
      textStyle: { textAlign: "center" }
    });
    const { success, message } = await login(email, password);
    if (isUsernameValid && isPasswordValid && success) {
      this.props.navigation.navigate("Drawer");
      Toast.show({
        text: "Login successfully!",
        duration: 2000,
        type: "success",
        textStyle: { textAlign: "center" }
      });
      const { actions } = this.props;
      await actions.getUserInfo();
      const { userInfo } = this.props;
      await actions.getBKCBalance(userInfo.userID);
      await actions.getETHBalance(userInfo.ethAddress);
    } else {
      Toast.show({
        text: `Login fail! ${message}`,
        duration: 2000,
        type: "danger",
        textStyle: { textAlign: "center" }
      });
    }
  }

  async signUpViaFB() {
    const { password, passwordConfirmation } = this.state;
    if (password === "") {
      alert("Password must not empty field");
      return;
    }
    if (password !== passwordConfirmation) {
      alert("Password mismatch");
      return;
    }
    try {
      const {
        type,
        token,
        expires,
        permissions,
        declinedPermissions
      } = await Facebook.logInWithReadPermissionsAsync("331134924419883", {
        permissions: ["public_profile", "email"]
      });
      Toast.show({
        text: "Waiting response from server",
        duration: 1000,
        type: "warning",
        textStyle: { textAlign: "center" }
      });
      if (type === "success") {
        // Get the user's name using Facebook's Graph API
        Toast.show({
          text: "Get information successfully!",
          duration: 2000,
          type: "success",
          textStyle: { textAlign: "center" }
        });
        const response = await fetch(
          `https://graph.facebook.com/me?fields=id,name,email&access_token=${token}`
        );
        const { email, name } = await response.json();
        const params = {
          email,
          password,
          name
        };
        const resp = await registerAccount(params);
        if (resp.success) {
          alert(
            `Email: ${email} sign up successfully, please check email for confirm link!`
          );
          Toast.show({
            text: `Email: ${email} sign up successfully, please check email for confirm link!`,
            duration: 2000,
            type: "success",
            textStyle: { textAlign: "center" }
          });
        } else {
          Toast.show({
            text: "Sign up fail! Error: " + resp.message,
            duration: 2000,
            type: "danger",
            textStyle: { textAlign: "center" }
          });
        }
      } else {
        // type === 'cancel'
      }
    } catch ({ message }) {
      Toast.show({
        text: "Get Facebook information fail! Error: " + message,
        duration: 2000,
        type: "danger",
        textStyle: { textAlign: "center" }
      });
    }
  }

  render() {
    const {
      selectedCategory,
      isLoading,
      isEmailValid,
      isUsernameValid,
      isPasswordValid,
      isConfirmationValid,
      email,
      password,
      passwordConfirmation
    } = this.state;
    const isLoginPage = selectedCategory === 0;
    const isSignUpPage = selectedCategory === 1;
    return (
      <View style={styles.container}>
        <ImageBackground source={BG_IMAGE} style={styles.bgImage}>
          {this.state.fontLoaded ? (
            <View>
              <KeyboardAvoidingView
                contentContainerStyle={styles.loginContainer}
                behavior="position"
              >
                <View style={styles.titleContainer}>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={styles.titleText}>Blockchain IoT</Text>
                  </View>
                  <View style={{ marginTop: -10, marginLeft: 10 }}>
                    <Text style={styles.titleText}>for Agriculture</Text>
                  </View>
                </View>
                <View style={{ flexDirection: "row" }}>
                  <Button
                    disabled={isLoading}
                    clear
                    activeOpacity={0.7}
                    onPress={() => this.selectCategory(0)}
                    containerStyle={{ flex: 1 }}
                    titleStyle={[
                      styles.categoryText,
                      isLoginPage && styles.selectedCategoryText
                    ]}
                    title={"Login"}
                  />
                  <Button
                    disabled={isLoading}
                    clear
                    activeOpacity={0.7}
                    onPress={() => this.selectCategory(1)}
                    containerStyle={{ flex: 1 }}
                    titleStyle={[
                      styles.categoryText,
                      isSignUpPage && styles.selectedCategoryText
                    ]}
                    title={"Sign up"}
                  />
                </View>
                <View style={styles.rowSelector}>
                  <TabSelector selected={isLoginPage} />
                  <TabSelector selected={isSignUpPage} />
                </View>
                <View style={styles.formContainer}>
                  {isLoginPage && (
                    <Input
                      leftIcon={
                        <Icon
                          name="envelope-o"
                          color="rgba(0, 0, 0, 0.38)"
                          size={25}
                          style={{ backgroundColor: "transparent" }}
                        />
                      }
                      value={email}
                      keyboardAppearance="light"
                      autoFocus={false}
                      autoCapitalize="none"
                      autoCorrect={false}
                      keyboardType="email-address"
                      returnKeyType="next"
                      inputStyle={{ marginLeft: 10 }}
                      placeholder={"Email"}
                      containerStyle={{
                        borderBottomColor: "rgba(0, 0, 0, 0.38)"
                      }}
                      ref={input => (this.usernameInput = input)}
                      onSubmitEditing={() => this.passwordInput.focus()}
                      onChangeText={email => this.setState({ email })}
                      errorMessage={
                        isUsernameValid ? null : "Please enter a valid email"
                      }
                    />
                  )}

                  <Input
                    leftIcon={
                      <SimpleIcon
                        name="lock"
                        color="rgba(0, 0, 0, 0.38)"
                        size={25}
                        style={{ backgroundColor: "transparent" }}
                      />
                    }
                    value={password}
                    keyboardAppearance="light"
                    autoCapitalize="none"
                    autoCorrect={false}
                    secureTextEntry={true}
                    returnKeyType={isSignUpPage ? "next" : "done"}
                    blurOnSubmit={true}
                    containerStyle={{
                      marginTop: 16,
                      borderBottomColor: "rgba(0, 0, 0, 0.38)"
                    }}
                    inputStyle={{ marginLeft: 10 }}
                    placeholder={"Password"}
                    ref={input => (this.passwordInput = input)}
                    onSubmitEditing={() =>
                      isSignUpPage
                        ? this.confirmationInput.focus()
                        : this.login()
                    }
                    onChangeText={password => this.setState({ password })}
                    errorMessage={
                      isPasswordValid
                        ? null
                        : "Please enter at least 8 characters"
                    }
                  />
                  {isSignUpPage && (
                    <Input
                      leftIcon={
                        <SimpleIcon
                          name="lock"
                          color="rgba(0, 0, 0, 0.38)"
                          size={25}
                          style={{ backgroundColor: "transparent" }}
                        />
                      }
                      value={passwordConfirmation}
                      secureTextEntry={true}
                      keyboardAppearance="light"
                      autoCapitalize="none"
                      autoCorrect={false}
                      keyboardType="default"
                      returnKeyType={"done"}
                      blurOnSubmit={true}
                      containerStyle={{
                        marginTop: 16,
                        borderBottomColor: "rgba(0, 0, 0, 0.38)"
                      }}
                      inputStyle={{ marginLeft: 10 }}
                      placeholder={"Confirm password"}
                      ref={input => (this.confirmationInput = input)}
                      onSubmitEditing={this.signUpViaFB}
                      onChangeText={passwordConfirmation =>
                        this.setState({ passwordConfirmation })
                      }
                      errorMessage={
                        isConfirmationValid
                          ? null
                          : "Please enter the same password"
                      }
                    />
                  )}
                  <Button
                    buttonStyle={styles.loginButton}
                    containerStyle={{ marginTop: 32, flex: 0 }}
                    activeOpacity={0.8}
                    title={isLoginPage ? "LOGIN" : "SIGN UP via Facebook"}
                    onPress={isLoginPage ? this.login : this.signUpViaFB}
                    titleStyle={styles.loginTextButton}
                    loading={isLoading}
                    disabled={isLoading}
                  />
                  {isLoginPage ? (
                    <Button
                      buttonStyle={styles.loginButton}
                      containerStyle={{ marginTop: 32, flex: 0 }}
                      activeOpacity={0.8}
                      title="LOGIN VIA FB"
                      onPress={this.logInViaFB}
                      titleStyle={styles.loginTextButton}
                      loading={isLoading}
                      disabled={isLoading}
                    />
                  ) : null}
                </View>
              </KeyboardAvoidingView>
              <View style={styles.helpContainer}>
                <Button
                  title={"Need help ?"}
                  titleStyle={{ color: "white" }}
                  buttonStyle={{ backgroundColor: "transparent" }}
                  underlayColor="transparent"
                  onPress={() => console.log("Account created")}
                />
              </View>
            </View>
          ) : (
            <Text>Loading...</Text>
          )}
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  rowSelector: {
    height: 20,
    flexDirection: "row",
    alignItems: "center"
  },
  selectorContainer: {
    flex: 1,
    alignItems: "center"
  },
  selected: {
    position: "absolute",
    borderRadius: 50,
    height: 0,
    width: 0,
    top: -5,
    borderRightWidth: 70,
    borderBottomWidth: 70,
    borderColor: "white",
    backgroundColor: "white"
  },
  loginContainer: {
    alignItems: "center",
    justifyContent: "center"
  },
  loginTextButton: {
    fontSize: 16,
    color: "white",
    fontWeight: "bold"
  },
  loginButton: {
    backgroundColor: "rgba(232, 147, 142, 1)",
    borderRadius: 10,
    height: 50,
    width: 200
  },
  titleContainer: {
    height: 150,
    backgroundColor: "transparent",
    justifyContent: "center"
  },
  formContainer: {
    backgroundColor: "white",
    width: SCREEN_WIDTH - 30,
    borderRadius: 10,
    paddingTop: 32,
    paddingBottom: 32,
    alignItems: "center"
  },
  loginText: {
    fontSize: 16,
    fontWeight: "bold",
    color: "white"
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    justifyContent: "center",
    alignItems: "center"
  },
  categoryText: {
    textAlign: "center",
    color: "white",
    fontSize: 24,
    fontFamily: "light",
    backgroundColor: "transparent",
    opacity: 0.54
  },
  selectedCategoryText: {
    opacity: 1
  },
  titleText: {
    color: "white",
    fontSize: 30,
    fontFamily: "regular"
  },
  helpContainer: {
    height: 64,
    alignItems: "center",
    justifyContent: "center"
  }
});

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({ ...userAction }, dispatch)
  };
};
const mapStateToProps = state => ({
  userInfo: state.userReducer.userInfo,
  bkcBalance: state.userReducer.bkcBalance
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
