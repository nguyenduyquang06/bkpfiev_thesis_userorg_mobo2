import * as React from "react";
import { Image } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Left,
  Body,
  Right,
  Picker,
  List,
  Icon,
  Thumbnail,
  ListItem,
  CardItem,
  Card,
  View,
} from "native-base";
import { SecureStore, MapView } from "expo";
import { getCropWithUID, getSmartFarmByCropID } from "../../../service/crop";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import _ from "lodash";

import styles from "./styles";
const BKLOGO = require("../../../../assets/bk_logo.png");
export interface Props {
  navigation: any;
  cropList: object[];
}
function calcMean(obj) {
  const latLongList = _.map(obj, (value, key) => {
    return value;
  });
  const latMean = _.meanBy(latLongList, (o) => o.Latitude);
  const longMean = _.meanBy(latLongList, (o) => o.Longitude);
  return {
    lat: latMean,
    long: longMean,
  };
}
export interface State {}
class Crops extends React.Component<Props, State> {
  state = {
    selectedCrop: {},
    cropList: [],
    smartFarmList: [],
    mapType: false, // true is satellite, else standard
  };

  async onValueChange(value: object) {
    const { data } = await getSmartFarmByCropID(
      this.props.navigation,
      value.id,
    );
    this.setState({
      selectedCrop: value,
      smartFarmList: data,
    });
  }
  async componentDidMount() {
    const token = await SecureStore.getItemAsync("secure_token");
    const { success, crops } = await getCropWithUID(this.props.navigation);
    if (success) {
      this.setState({
        cropList: crops,
        selectedCrop: crops[0],
      });
    }
    const { data } = await getSmartFarmByCropID(
      this.props.navigation,
      crops[0].id,
    );
    this.setState({
      smartFarmList: data,
    });
  }

  render() {
    const { smartFarmList, selectedCrop } = this.state;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent>
              <Icon
                active
                name="menu"
                onPress={() => this.props.navigation.openDrawer()}
              />
            </Button>
          </Left>
          <Body>
            <Title>Crops</Title>
          </Body>
          <Right />
        </Header>
        <Content>
          <View
            style={{
              justifyContent: "center",
              flexDirection: "row",
              flexWrap: "wrap",
            }}
          >
            <Picker
              mode="dropdown"
              placeholder="Select crop"
              iosIcon={<Icon name="arrow-down" />}
              textStyle={{ color: "#5cb85c" }}
              itemTextStyle={{ color: "#788ad2" }}
              style={{
                width: 200,
                alignSelf: "center",
                alignItems: "flex-end",
              }}
              selectedValue={this.state.selectedCrop}
              onValueChange={this.onValueChange.bind(this)}
            >
              {_.map(this.state.cropList, (o) => (
                <Picker.Item label={o.id} key={o.id} value={o} />
              ))}
            </Picker>
            <IconMaterial.Button
              onPress={() => {
                this.props.navigation.navigate("CropCreate");
              }}
              color="#0051ff"
              backgroundColor="#FBFAFA"
              size={30}
              name="plus-circle"
            />
            <IconMaterial.Button
              onPress={() => {
                this.props.navigation.navigate("CropCreate");
              }}
              color="#0051ff"
              // backgroundColor="#FBFAFA"
              style={{ backgroundColor: "#FBFAFA" }}
              size={30}
              name="bandcamp"
            />
          </View>
          {_.map(smartFarmList, (o) => {
            const { long, lat } = calcMean(o.position);
            const latLongList = _.map(o.position, (p, key) => ({
              latitude: p.Latitude,
              longitude: p.Longitude,
            }));
            return (
              <Card key={o.blockID} style={{ flex: 0 }}>
                <CardItem>
                  <Left>
                    <Thumbnail square source={BKLOGO} />
                    <Body>
                      <View
                        padder
                        style={{ flexDirection: "row", flexWrap: "wrap" }}
                      >
                        <Text>{o.blockID}</Text>
                        <Right>
                          <IconMaterial.Button
                            onPress={() => {
                              this.setState({ mapType: !this.state.mapType });
                            }}
                            color="#0051ff"
                            backgroundColor="#fff"
                            size={30}
                            name="eye"
                          />
                        </Right>
                      </View>
                      <Text note>{`${o.FromDate} - ${
                        o.ToDate ? o.ToDate : "Not yet update"
                      }`}</Text>
                    </Body>
                  </Left>
                </CardItem>
                <View>
                  <MapView
                    style={{ alignSelf: "stretch", height: 300 }}
                    liteMode={true}
                    mapType={this.state.mapType ? "satellite" : "standard"}
                    initialRegion={{
                      latitude: lat,
                      longitude: long,
                      latitudeDelta: 0.0005,
                      longitudeDelta: 0.0005,
                    }}
                  >
                    {/* {_.map(latLongList, ({latitude,longitude}) => (
                      <MapView.Marker
                        coordinate={{
                          latitude,
                          longitude
                        }}
                        key={longitude}
                        title={o.blockID}
                        description={`Smart Farm ID: ${o.blockID}`}
                      />
                    ))} */}
                    <MapView.Polygon
                      coordinates={latLongList}
                      strokeWidth={2}
                      fillOpacity={1}
                      draggable
                      editable
                      strokeColor="#215732"
                      fillColor="#368e52"
                    />
                  </MapView>
                </View>
                <CardItem>
                  <Body>
                    {/* <Image
                      source={{ uri: "Image URL" }}
                      style={{ height: 100, width: 100, flex: 1 }}
                    /> */}

                    <Text>{o.NodeIDList.length} node active</Text>
                    <Text>
                      Lat.{lat.toFixed(6)} , Long.{long.toFixed(6)}
                    </Text>
                  </Body>
                </CardItem>
                <CardItem>
                  <Left>
                    <Button transparent textStyle={{ color: "#87838B" }}>
                      <Icon
                        style={{ fontSize: 50 }}
                        name="ios-information-circle"
                        onPress={() => {
                          this.props.navigation.navigate("Details");
                        }}
                      />
                      <Text>Details</Text>
                    </Button>
                  </Left>
                  <Right>
                    <Button transparent textStyle={{ color: "#87838B" }}>
                      <IconMaterial size={50} name="resistor-nodes" />
                      <Text>View Node Info</Text>
                    </Button>
                  </Right>
                </CardItem>
              </Card>
            );
          })}
        </Content>
      </Container>
    );
  }
}

export default Crops;
