import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Button,
  Right,
  Body,
  Tab,
  Tabs,
  View,
  List,
  ListItem,
  Picker,
  Icon,
  Row,
  Col
} from "native-base";
import { connect } from "react-redux";
import { Input } from "react-native-elements";
import { AirbnbRating } from "react-native-ratings";
import styles from "./styles";
import IconMaterial from "react-native-vector-icons/MaterialCommunityIcons";
import {
  getProductLine,
  getCoffeeBatch,
  getProduct,
  getCrop,
  submitReview,
  getReviews,
  getCertOfOrigin
} from "../../../service/product";
import { Image } from "react-native";
import { getHistoryCoffeeBatch } from "../../../service/coffeeBatch";
import { getNodeIDList } from "../../../service/smartFarm";
import { CERTIFICATE } from "./license.js";
import _ from "lodash";
import moment from "moment";
import Timeline from "react-native-timeline-listview";
import { TouchableOpacity, Linking } from "react-native";

export interface Props {
  navigation: any;
}

export interface State {}
class ProductInformation extends React.Component<Props, State> {
  state = {
    intro: "",
    ingredient: "",
    certificate: {},
    characteristics: [],
    certType: CERTIFICATE[2], // display Food safety default
    weight: null,
    roastType: "",
    mfgDate: moment().format(),
    expDate: moment().format(),
    price: null,
    openTimeline: false,
    crops: [],
    score: null,
    selected: null,
    status: "",
    coffeeHistData: [],
    rating: 8,
    coffeeBatch: {},
    //product
    batchProductID: "",
    ratio: null,
    batchProductStatus: null,
    totalProduct: null,
    availableProduct: null,
    failProduct: null,
    soldProduct: null,
    usedProduct: null,
    grossRevenue: null,
    productLineID: "",
    ownerProduct: "",
    productID: "",
    comment: "",
    code: "",
    reviews: [],
    product: {},
    coffeeBatchs: []
  };

  constructor(props) {
    super(props);
    this.onEventPress = this.onEventPress.bind(this);
    this.renderDetail = this.renderDetail.bind(this);
  }
  onEventPress(data: any) {
    this.setState({ selected: data });
  }

  renderDetail(rowData) {
    const title = <Text style={[styles.title]}>{rowData.title}</Text>;
    let desc = null;
    if (rowData.description) {
      desc = (
        <View>
          <Text style={[styles.textDescription]}>{rowData.description}</Text>
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }}>
        {title}
        {desc}
      </View>
    );
  }

  async componentDidMount() {
    const param = this.props.navigation.state.params;
    const {
      PRODUCT: { uniqueProductID }
    } = param;
    this.getProductInfo(uniqueProductID);
  }

  async getHistoryChangeLogCoffeeBatch(coffeeBatch) {
    const resp = await getHistoryCoffeeBatch(this.props.navigation, {
      cropID: coffeeBatch.cropID,
      blockID: coffeeBatch.blockID,
      batchID: coffeeBatch.batchID,
      growerID: coffeeBatch.growerID,
      growerMSPID: coffeeBatch.growerMSPID
    });
    if (!resp.success) {
      alert(
        "Fail to get " +
          coffeeBatch.batchID +
          " batch information due to " +
          resp.message
      );
      return;
    }
    const coffeeBatchList = JSON.parse(resp.message);
    const differences = _.reduce(
      Array(coffeeBatchList.length - 1),
      (result, o, key) => {
        const obj = {
          timestamp: coffeeBatchList[key + 1].timestamp,
          diffs: Object.keys(coffeeBatchList[key].value).filter(
            k =>
              JSON.stringify(coffeeBatchList[key].value[k]) !==
              JSON.stringify(coffeeBatchList[key + 1].value[k])
          )
        };
        if (_.isEmpty(obj.diffs)) {
          return result;
        }
        return [...result, obj];
      },
      []
    );
    const mappingChangeToText = _.map(differences, (o, key) => {
      const diffsText = _.map(o.diffs, diff => {
        switch (diff) {
          case "owner":
            return `Change owner from ${coffeeBatchList[key].value.owner} to ${
              coffeeBatchList[key + 1].value.owner
            }`;
          case "carrierID":
            return "Edit carrier ID";
          case "carrierMSPID":
            return "Edit carrier MSP ID";
          case "exporterID":
            return "Add Exporter ID";
          case "exporterMSPID":
            return "Add Exporter MSP ID";
          case "packingList":
            return "Submit Packing list";
          case "importerID":
            return "Add Importer ID";
          case "importerMSPID":
            return "Add Importer MSP ID";
          case "batchState":
            return "Edit batch status";
          case "factoryID":
            return "Add Factory ID";
          case "factoryMSPID":
            return "Add Factory MSPID";
          case "receivedShipment":
            return "Submit received shipment information";
          case "contractStepOneID":
            return "Link contract step one to Coffee Batch";
          case "contractStepTwoID":
            return "Link contract step two to Coffee Batch";
          case "contractStepThreeID":
            return "Link contract step three to Coffee Batch";
          case "batchProductID":
            return "Add batch product";
          case "batchStatePerProductLine":
            return "Edit batch state product";
          default:
            return "Developing";
        }
      });
      return {
        time: `${moment.unix(o.timestamp).format("DD-MM-YYYY")}\n${moment
          .unix(o.timestamp)
          .format("HH:mm:ss")}`,
        description: _.join(diffsText, "\n"),
        title: "Change " + ++key
      };
    });
    const coffeeHistData = [
      {
        time: `${moment
          .unix(coffeeBatchList[0].timestamp)
          .format("DD-MM-YYYY")}\n${moment
          .unix(coffeeBatchList[0].timestamp)
          .format("HH:mm:ss")}`,
        description: "Creating new coffee batch",
        title: "Change 0"
      },
      ...mappingChangeToText
    ];
    return coffeeHistData;
  }

  async getProductInfo(uniqueProductID) {
    const { bkcBalance } = this.props;
    const productLineID = uniqueProductID.productLineID;
    const batchID = uniqueProductID.batchProductID;
    const productID = uniqueProductID.productID;
    const productLine = await getProductLine(productLineID, batchID);
    const batchProduct = _.find(
      productLine.listBatch,
      o => o.ID === batchID,
      0
    );
    const { rawCoffeeBatchIDs } = batchProduct;
    const product = await getProduct(uniqueProductID);
    const cropParams = _.map(rawCoffeeBatchIDs, o => ({
      growerID: o.growerID,
      growerMSPID: o.growerMSPID,
      id: o.cropID
    }));
    const crops = await getCrop(cropParams);
    const coffeeBatchs = await getCoffeeBatch(rawCoffeeBatchIDs);
    const coffeeBatch = coffeeBatchs[0];
    const listContractStepTwoID = _.map(coffeeBatchs, o => o.contractStepTwoID);
    Promise.all(
      _.map(listContractStepTwoID, async o => await getCertOfOrigin(o))
    ).then(res => {
      const certificate = {
        certOfOrigin: res,
        organicCert: batchProduct.organicCert,
        foodSafety: batchProduct.foodSafety
      };
      this.setState({
        certificate
      });
    });
    this.setState({
      coffeeHistData: await this.getHistoryChangeLogCoffeeBatch(coffeeBatch)
    });
    this.setState({
      productLineID,
      intro: productLine.intro,
      crops,
      coffeeBatchs,
      coffeeBatch,
      ingredient: productLine.ingredient,
      characteristics: productLine.characteristics,
      weight: productLine.weight,
      roastType: productLine.roastType,
      mfgDate: batchProduct.mfgDate,
      expDate: batchProduct.expDate,
      price: product.price,
      status: product.status,
      score: product.score,
      batchProductID: batchID,
      ratio: batchProduct.ratio,
      batchProductStatus: batchProduct.status,
      totalProduct: batchProduct.totalProduct,
      availableProduct: batchProduct.productAvailable,
      failProduct: batchProduct.productFail,
      soldProduct: batchProduct.productSold,
      usedProduct: batchProduct.productUsed,
      grossRevenue: product.price * batchProduct.productSold,
      productID,
      batchID: batchProduct.batchID,
      ownerProduct:
        product.userID === bkcBalance.address
          ? "You're owner"
          : product.status === "SOLD"
          ? "Other customer"
          : "Factory",
      reviews: await getReviews(productLineID),
      product
    });
  }

  async onSubmitReview() {
    const {
      productLineID,
      batchProductID,
      productID,
      rating,
      comment,
      code
    } = this.state;
    const params = {
      uniqueProductID: {
        productLineID,
        batchProductID,
        productID
      },
      score: rating,
      comment,
      passcode: code
    };
    const resp = await submitReview(params);
    if (resp.success) {
      alert(
        "Submit review successfully at transaction ID " + resp.transactionID
      );
      this.getProductInfo({ productLineID, batchProductID, productID });
      return;
    }
    alert("Submit fail due to " + resp.message);
  }

  async onValueChange(coffeeBatch) {
    this.setState({
      coffeeBatch,
      coffeeHistData: await this.getHistoryChangeLogCoffeeBatch(coffeeBatch)
    });
  }

  onValueChangeCertType(certType) {
    this.setState({
      certType
    });
  }

  render() {
    const {
      intro,
      ingredient,
      characteristics,
      weight,
      mfgDate,
      expDate,
      price,
      reviews,
      score,
      roastType,
      crops,
      status,
      batchProductID,
      ratio,
      batchProductStatus,
      totalProduct,
      availableProduct,
      failProduct,
      soldProduct,
      usedProduct,
      grossRevenue,
      ownerProduct,
      product,
      productID,
      productLineID,
      comment,
      code,
      coffeeBatchs,
      certificate,
      certType,
      coffeeBatch
    } = this.state;
    const { bkcBalance } = this.props;
    return (
      <Container style={styles.container}>
        <Header>
          <Body style={{ flex: 3 }}>
            <Title>Retrieve Product Info</Title>
          </Body>
        </Header>
        <Tabs locked>
          <Tab heading="General">
            <Content padder>
              <Text style={{ ...styles.key, fontSize: 30 }}>
                {productLineID}
              </Text>
              <View style={{ flexDirection: "column" }}>
                <View>
                  <Text style={{ ...styles.key, fontSize: 30 }}>
                    {price} BKC
                  </Text>
                </View>
                {status === "AVAILABLE" ? (
                  <View
                    style={{
                      justifyContent: "flex-end",
                      alignSelf: "flex-end",
                      alignItems: "flex-end"
                    }}
                  >
                    <Button
                      onPress={() => {
                        if (_.isEmpty(bkcBalance)) {
                          alert("Wallet not found, you need create it first");
                          return;
                        }
                        if (bkcBalance.balance < price) {
                          alert(
                            "You don't have enough balance to buy this product"
                          );
                          return;
                        }
                        this.props.navigation.navigate("Checkout", {
                          price,
                          quantity: 1,
                          PRODUCT: {
                            uniqueProductID: {
                              batchProductID,
                              productID,
                              productLineID
                            }
                          },
                          onSuccessPay: this.getProductInfo.bind(this)
                        });
                      }}
                      transparent
                      // style={{height:300}}
                      textStyle={{ color: "#87838B" }}
                    >
                      <IconMaterial size={75} color="#0051ff" name="cash" />
                    </Button>
                    <Text
                      style={{
                        ...styles.key,
                        color: "#0051ff",
                        fontSize: 25,
                        marginRight: 20
                      }}
                    >
                      Pay
                    </Text>
                  </View>
                ) : status === "SOLD" &&
                  bkcBalance.address === product.userID ? (
                  <View
                    style={{
                      justifyContent: "flex-end",
                      alignSelf: "flex-end",
                      alignItems: "flex-end"
                    }}
                  >
                    <Button
                      onPress={() => {
                        this.props.navigation.navigate("Refund", {
                          price,
                          quantity: 1,
                          PRODUCT: {
                            uniqueProductID: {
                              batchProductID,
                              productID,
                              productLineID,
                            }
                          },
                          onSuccessRefund: this.getProductInfo.bind(this)
                        });
                      }}
                      transparent
                      style={{alignSelf:"center"}}
                      textStyle={{ color: "#87838B" }}
                    >
                      <IconMaterial size={75} color="#0051ff" name="cash" />
                    </Button>
                    <Text
                      style={{
                        ...styles.key,
                        color: "#0051ff",
                        fontSize: 25,
                        marginRight: 20,                        
                      }}
                    >
                      Refund
                    </Text>
                  </View>
                ) : null}
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="leaf" />
                <Text style={styles.key}>Product:</Text>
                <Text style={styles.value}>{productLineID}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="leaf" />
                <Text style={styles.key}>ID:</Text>
                <Text style={styles.value}>{productID}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="leaf" />
                <Text style={styles.key}>Introduction:</Text>
                <Text style={styles.value}>{intro}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="leaf" />
                <Text style={styles.key}>Origin:</Text>
                <Text style={styles.value}>
                  {" "}
                  {_.map(crops, o => o.origin).toString()}{" "}
                </Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="domain" />
                <Text style={styles.key}>Ingredient:</Text>
                <Text style={styles.value}>{ingredient}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="account-circle" />
                <Text style={styles.key}>Characteristics:</Text>
                <Text style={styles.value}>{characteristics.toString()}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="home-automation" />
                <Text style={styles.key}>Roast Type:</Text>
                <Text style={styles.value}>{roastType}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="home-automation" />
                <Text style={styles.key}>Weight:</Text>
                <Text style={styles.value}>{weight}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="home-automation" />
                <Text style={styles.key}>Manufacture Date:</Text>
                <Text style={styles.value}>{mfgDate}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="home-automation" />
                <Text style={styles.key}>Expiration Date:</Text>
                <Text style={styles.value}> {expDate} </Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="home-automation" />
                <Text style={styles.key}>Company:</Text>
                <Text style={styles.value}>FactoryOrg</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="home-automation" />
                <Text style={styles.key}>Price:</Text>
                <Text style={styles.value}>{price} BKC</Text>
              </View>

              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="coffee" />
                <Text style={styles.key}>Owner</Text>
                <Text style={styles.value}>{ownerProduct}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="coffee" />
                <Text style={styles.key}>Status</Text>
                <Text style={styles.value}>{status}</Text>
              </View>
            </Content>
          </Tab>
          <Tab heading="Cert">
            <Content padder>
              <Text style={{ ...styles.key, fontSize: 30 }}>Certificate</Text>
              <Picker
                mode="dropdown"
                placeholder="Select crop"
                iosIcon={<Icon name="arrow-down" />}
                textStyle={{ color: "#5cb85c" }}
                itemTextStyle={{ color: "#788ad2" }}
                style={{
                  width: 150,
                  alignSelf: "center",
                  alignItems: "flex-end"
                }}
                selectedValue={this.state.certType}
                onValueChange={this.onValueChangeCertType.bind(this)}
              >
                <Picker.Item label={"None"} value={"None"} />
                {_.map(CERTIFICATE, o => (
                  <Picker.Item label={o.label} key={o.type} value={o} />
                ))}
              </Picker>
              {certType.type === "certOfOrigin" ? (
                <View>
                  {_.map(_.get(certificate, [certType.type]), (o, okey) => (
                    <View key={okey}>
                      <Text style={{ ...styles.key, fontSize: 30 }}>
                        {`${certType.label} ${okey + 1}`}
                      </Text>
                      {_.map(certType.content, (val, key) => (
                        <View
                          padder
                          key={key}
                          style={{ flexDirection: "row", flexWrap: "wrap" }}
                        >
                          <IconMaterial size={30} name="leaf" />
                          <Text style={styles.key}>{val}</Text>
                          {key === "link" ? (
                            <TouchableOpacity
                              onPress={() => Linking.openURL(_.get(o, key))}
                            >
                              <Text style={{ color: "blue" }}>
                                Link to browser
                              </Text>
                              <Image
                                source={{
                                  uri: _.get(o, key)
                                }}
                                style={{ height: 300, width: 300, flex: 1 }}
                              />
                            </TouchableOpacity>
                          ) : (
                            <Text style={styles.value}>{_.get(o, key)}</Text>
                          )}
                        </View>
                      ))}
                    </View>
                  ))}
                </View>
              ) : (
                <View>
                  <Text style={{ ...styles.key, fontSize: 30 }}>
                    {certType.label}
                  </Text>
                  {_.map(certType.content, (val, key) => (
                    <View
                      padder
                      key={key}
                      style={{ flexDirection: "row", flexWrap: "wrap" }}
                    >
                      <IconMaterial size={30} name="leaf" />
                      <Text style={styles.key}>{val}:</Text>
                      {key === "link" ? (
                        <TouchableOpacity
                          onPress={() =>
                            Linking.openURL(
                              _.get(certificate, [certType.type, key])
                            )
                          }
                        >
                          <Text style={{ color: "blue" }}>Link to browser</Text>
                          <Image
                            source={{
                              uri: _.get(certificate, [certType.type, key])
                            }}
                            style={{ height: 300, width: 300, flex: 1 }}
                          />
                        </TouchableOpacity>
                      ) : (
                        <Text style={styles.value}>
                          {_.get(certificate, [certType.type, key])}
                        </Text>
                      )}
                    </View>
                  ))}
                </View>
              )}
            </Content>
          </Tab>
          <Tab heading="Batch">
            <Content padder>
              <Text style={{ ...styles.key, fontSize: 30 }}>Product Batch</Text>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="leaf" />
                <Text style={styles.key}>Batch Product ID:</Text>
                <Text style={styles.value}>{batchProductID}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="leaf" />
                <Text style={styles.key}>Ratio:</Text>
                <Text style={styles.value}>{ratio}</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="leaf" />
                <Text style={styles.key}>Status:</Text>
                <Text style={styles.value}> {batchProductStatus} </Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="domain" />
                <Text style={styles.key}>Total Product:</Text>
                <Text style={styles.value}>{totalProduct} products</Text>
              </View>
              <Text style={{ ...styles.key, fontSize: 30 }}>
                Sale Management
              </Text>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="account-circle" />
                <Text style={styles.key}>Available:</Text>
                <Text style={styles.value}>{availableProduct} products</Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="home-automation" />
                <Text style={styles.key}>Failure:</Text>
                <Text style={styles.value}>{failProduct} products </Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="home-automation" />
                <Text style={styles.key}>Sold:</Text>
                <Text style={styles.value}>{soldProduct} products </Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="home-automation" />
                <Text style={styles.key}>Used:</Text>
                <Text style={styles.value}> {usedProduct} products </Text>
              </View>
              <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                <IconMaterial size={30} name="home-automation" />
                <Text style={styles.key}>Gross Revenue:</Text>
                <Text style={styles.value}> {grossRevenue} BKC </Text>
              </View>
            </Content>
          </Tab>

          <Tab heading="Track">
            <Content>
              <Picker
                mode="dropdown"
                placeholder="Select coffee batch"
                iosIcon={<Icon name="arrow-down" />}
                textStyle={{ color: "#5cb85c" }}
                itemTextStyle={{ color: "#788ad2" }}
                style={{
                  width: 150,
                  alignSelf: "center",
                  alignItems: "flex-end"
                }}
                selectedValue={this.state.coffeeBatch}
                onValueChange={this.onValueChange.bind(this)}
              >
                <Picker.Item label={"None"} value={"None"} />
                {_.map(this.state.coffeeBatchs, o => (
                  <Picker.Item label={o.batchID} key={o._id} value={o} />
                ))}
              </Picker>
              <Content padder>
                <Text style={{ ...styles.key, fontSize: 30 }}>
                  Batch Raw Coffee
                </Text>
                <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <IconMaterial size={30} name="leaf" />
                  <Text style={styles.key}>Crop ID:</Text>
                  <Text style={styles.value}>{coffeeBatch.cropID}</Text>
                </View>
                <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <IconMaterial size={30} name="leaf" />
                  <Text style={styles.key}>Block ID:</Text>
                  <Text style={styles.value}>{coffeeBatch.blockID}</Text>
                </View>
                <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <IconMaterial size={30} name="leaf" />
                  <Text style={styles.key}>Batch ID:</Text>
                  <Text style={styles.value}> {coffeeBatch.batchID} </Text>
                </View>
                <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <IconMaterial size={30} name="domain" />
                  <Text style={styles.key}>Crop Date:</Text>
                  <Text style={styles.value}>{coffeeBatch.cropDate}</Text>
                </View>
                <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <IconMaterial size={30} name="account-circle" />
                  <Text style={styles.key}>Owner:</Text>
                  <Text style={styles.value}>{coffeeBatch.owner}</Text>
                </View>
                <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <IconMaterial size={30} name="home-automation" />
                  <Text style={styles.key}>Weight:</Text>
                  <Text style={styles.value}>{coffeeBatch.weight}</Text>
                </View>
                <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <IconMaterial size={30} name="home-automation" />
                  <Text style={styles.key}>Fair Trade RP:</Text>
                  <Text style={styles.value}>
                    {_.get(coffeeBatch, ["fairTradeReport", "documentURL"])}
                  </Text>
                </View>
              </Content>
              <Content>
                <Row>
                  <Col>
                    <Button
                      onPress={() => {
                        this.setState({
                          openTimeline: !this.state.openTimeline
                        });
                      }}
                      style={{ alignSelf: "center" }}
                      primary
                    >
                      <Text>
                        {this.state.openTimeline
                          ? "Hide Timeline"
                          : "Show Timeline"}
                      </Text>
                    </Button>
                  </Col>
                  <Col>
                    <Button
                      onPress={() => {
                        this.props.navigation.navigate("TrackingPath", {
                          contractStepOneID: _.get(
                            coffeeBatch,
                            "contractStepOneID"
                          ),
                          contractStepTwoID: _.get(
                            coffeeBatch,
                            "contractStepTwoID"
                          ),
                          contractStepThreeID: _.get(
                            coffeeBatch,
                            "contractStepThreeID"
                          )
                        });
                      }}
                      style={{ alignSelf: "center" }}
                      primary
                    >
                      <Text>Tracking Path</Text>
                    </Button>
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <Button
                      onPress={async () => {
                        const nodeIDList = await getNodeIDList({
                          cropID: _.get(coffeeBatch, "cropID"),
                          blockID: _.get(coffeeBatch, "blockID")
                        });
                        this.props.navigation.navigate("NodesInfo", {
                          nodeIDList,
                          coffeeBatch
                        });
                      }}
                      style={{ alignSelf: "center" }}
                      primary
                    >
                      <Text>Farm Tracking</Text>
                    </Button>
                  </Col>
                </Row>
                {this.state.openTimeline ? (
                  <Timeline
                    style={styles.list}
                    data={this.state.coffeeHistData}
                    circleSize={20}
                    circleColor="rgba(0,0,0,0)"
                    lineColor="rgb(45,156,219)"
                    timeContainerStyle={{ minWidth: 52, marginTop: -5 }}
                    timeStyle={{
                      textAlign: "center",
                      backgroundColor: "#ff9797",
                      color: "white",
                      padding: 5,
                      borderRadius: 13
                    }}
                    descriptionStyle={{ color: "gray" }}
                    options={{
                      style: { paddingTop: 5 }
                    }}
                    innerCircle={"icon"}
                    onEventPress={this.onEventPress}
                    renderDetail={this.renderDetail}
                    separator={false}
                    detailContainerStyle={{
                      marginBottom: 20,
                      paddingLeft: 5,
                      paddingRight: 5,
                      backgroundColor: "#BBDAFF",
                      borderRadius: 10
                    }}
                    columnFormat="single-column-left"
                  />
                ) : null}
              </Content>
            </Content>
          </Tab>
          <Tab heading="Reviews">
            {score === 0 && status === "SOLD" ? (
              <View style={{ alignItems: "center" }}>
                <AirbnbRating
                  count={10}
                  reviews={[
                    "Terrible",
                    "Bad",
                    "Meh",
                    "OK",
                    "Good",
                    "Hmm...",
                    "Very Good",
                    "Wow",
                    "Amazing",
                    "Unbelievable"
                  ]}
                  defaultRating={8}
                  size={30}
                  onFinishRating={rating => this.setState({ rating })}
                />
                <Input
                  // containerStyle={{ width: "20%" }}
                  label="Comment"
                  onChangeText={comment => this.setState({ comment })}
                  labelStyle={{ marginTop: 16 }}
                />
                <Input
                  containerStyle={{ width: "20%" }}
                  label="Code"
                  onChangeText={code => this.setState({ code })}
                  labelStyle={{ marginTop: 16 }}
                />
                <Button
                  onPress={() => {
                    if (comment === "" || code === "") {
                      alert("Fill in require fields");
                      return;
                    }
                    this.onSubmitReview();
                  }}
                  style={{ alignSelf: "center" }}
                  primary
                >
                  <Text>Submit Score</Text>
                </Button>
              </View>
            ) : score !== 0 &&
              status === "USED" &&
              ownerProduct === "You're owner" ? (
              <View>
                <Text style={{ ...styles.key, fontSize: 30 }}>Your review</Text>
                <AirbnbRating
                  count={10}
                  reviews={[
                    "Terrible",
                    "Bad",
                    "Meh",
                    "OK",
                    "Good",
                    "Hmm...",
                    "Very Good",
                    "Wow",
                    "Amazing",
                    "Unbelievable"
                  ]}
                  isDisabled
                  defaultRating={product.score}
                  size={30}
                />
                <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <Text style={styles.key}>Comment:</Text>
                  <Text style={styles.value}>{product.comment}</Text>
                </View>
                <View padder style={{ flexDirection: "row", flexWrap: "wrap" }}>
                  <Text style={styles.key}>At:</Text>
                  <Text style={styles.value}>
                    {moment
                      .unix(product.reviewTime)
                      .format("MMMM Do YYYY, h:mm:ss a")}
                  </Text>
                </View>
              </View>
            ) : null}
            {reviews.length === 0 ? (
              <Text>No reviews</Text>
            ) : (
              <Content>
                <Text style={{ ...styles.key, fontSize: 30 }}>Reviews</Text>
                <List>
                  {_.map(reviews, o => (
                    <ListItem key={o.reviewTime} avatar>
                      <Body>
                        <Text numberOfLines={1}>{o.userID}</Text>
                        <Text note>{o.comment}</Text>
                      </Body>
                      <Right>
                        <Text note>
                          {moment.unix(o.reviewTime).format("MM-DD-YYYY")}
                        </Text>
                        <Text>{o.score} stars</Text>
                      </Right>
                    </ListItem>
                  ))}
                </List>
              </Content>
            )}
          </Tab>
        </Tabs>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  bkcBalance: state.userReducer.bkcBalance
});

export default connect(mapStateToProps)(ProductInformation);
