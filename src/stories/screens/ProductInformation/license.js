export const CERTIFICATE = [
  {  // type 0 
    type:"certOfOrigin",
    label:"Cert Of Origin",
    content:{
      certNumber:"Certificate No.",
      exporter:"Exporter",
      importer:"Importer",
      dateOfExport:"Date of Export",
      countryOfOrigin:"Country of Origin",
      destination:"Destination",
      issuedDate:"Issued Date",
      link:"Ref",
    }
  },
  { // type 1
    type:"organicCert",
    label:"Organic Cert",
    content:{
      certNumber:"Certificate No.",
      initDate:"Initial Date",
      anniversaryDate:"Anniversary Date",
      issuedDate:"Issued Date",
      link:"Ref",
    }
  },
  { // type 2
    type:"foodSafety",
    label:"Food Safety",
    content:{
      certNumber:"Certificate No.",
      dateOfCertDecision:"Date of Certificate decision",
      initDate:"Initial Date",
      expDate:"Expiration Date",
      scope:"Scope",
      category:"Category",
      owner:"Owner",
      address:"Address",
      link:"Ref",
    }
  },
];