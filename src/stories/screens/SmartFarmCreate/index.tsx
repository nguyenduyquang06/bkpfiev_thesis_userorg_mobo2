import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Form,
  Item,
  Label,
  Input,
  Button,
  Icon,
  Left,
  Right,
  Body,
  Toast,
  View,
  DatePicker,
} from "native-base";
import styles from "./styles";
import { Col, Row, Grid } from "react-native-easy-grid";
import { initSmartFarm } from "../../../service/smartFarm";
import moment from "moment";
import { connect } from "react-redux";
export interface Props {
  navigation: any;
}

export interface State {
  cropID: string;
  blockID: string;
  latTopLeft: number;
  longTopLeft: number;
  latTopRight: number;
  longTopRight: number;
  latBottomRight: number;
  longBottomRight: number;
  latBottomLeft: number;
  longBottomLeft: number;
  chosenDate: object;
}

export interface State {}
class SmartFarmCreate extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      chosenDate: moment(),
      cropID: "",
      blockID: "",
      latTopLeft: 0,
      longTopLeft: 0,
      latTopRight: 0,
      longTopRight: 0,
      latBottomRight: 0,
      longBottomRight: 0,
      latBottomLeft: 0,
      longBottomLeft: 0,
    };
  }

  async onSubmitNewCrop() {
    const { selectedCrop } = this.props;
    const {
      blockID,
      chosenDate,
      latTopLeft,
      longTopLeft,
      latTopRight,
      longTopRight,
      latBottomRight,
      longBottomRight,
      latBottomLeft,
      longBottomLeft,
    } = this.state;
    const success = await initSmartFarm(
      this.props.navigation,
      selectedCrop.id,
      blockID,
      moment(chosenDate).format(),
      latTopLeft.toString(),
      longTopLeft.toString(),
      latTopRight.toString(),
      longTopRight.toString(),
      latBottomRight.toString(),
      longBottomRight.toString(),
      latBottomLeft.toString(),
      longBottomLeft.toString(),
    );

    if (success) {
      Toast.show({
        text: `Create ${blockID} successfully`,
        duration: 2000,
        type: "success",
        textStyle: { textAlign: "center" },
      });
    }
  }
  onRegionSelected({region}) {
    this.setState({
      latTopLeft:region[0].latitude,
      longTopLeft:region[0].longitude,
      latTopRight:region[1].latitude,
      longTopRight:region[1].longitude,
      latBottomRight:region[2].latitude,
      longBottomRight: region[2].longitude,
      latBottomLeft: region[3].latitude,
      longBottomLeft: region[3].longitude,
    })
  }
  render() {
    const { selectedCrop } = this.props;
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="ios-arrow-back" />
            </Button>
          </Left>
          <Body style={{ flex: 3 }}>
            <Title>New SmartFarm</Title>
          </Body>
          <Right />
        </Header>

        <Content padder>
          <Form>
            <Grid>
              <Row>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Crop ID</Label>
                    <Input value={selectedCrop.id} disabled />
                  </Item>
                </Col>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Block ID</Label>
                    <Input
                      value={this.state.blockID}
                      onChangeText={(value) => this.setState({ blockID: value })}
                    />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Item stackedLabel>
                    <Label style={styles.key}>Made from date</Label>
                    <View>
                      <DatePicker
                        locale={"en"}
                        timeZoneOffsetInMinutes={undefined}
                        modalTransparent={false}
                        animationType={"fade"}
                        androidMode={"default"}
                        placeHolderText="Date"
                        textStyle={{ fontSize: 15, color: "black" }}
                        placeHolderTextStyle={{
                          fontSize: 15,
                          color: "#d3d3d3",
                        }}
                        onDateChange={(newDate) => {
                          // console.log(moment(newDate).format())
                          this.setState({ chosenDate: newDate });
                        }}
                        disabled={false}
                      />
                    </View>
                  </Item>
                </Col>
              </Row>
              <Row>
                <Label style={styles.key}> Top Left: </Label>
              </Row>
              <Row>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Latitude</Label>
                    <Input
                      value={
                        this.state.latTopLeft
                          ? this.state.latTopLeft.toString()
                          : ""
                      }
                      onChangeText={(text) => {
                        this.setState({
                          latTopLeft: parseFloat(
                            text.replace(/[^(\d+)\.(\d+)]/g, ""),
                          ),
                        });
                      }}
                      keyboardType="numeric"
                    />
                  </Item>
                </Col>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Longitude</Label>
                    <Input
                      value={
                        this.state.longTopLeft
                          ? this.state.longTopLeft.toString()
                          : ""
                      }
                      onChangeText={(text) => {
                        this.setState({
                          longTopLeft: parseFloat(
                            text.replace(/[^(\d+)\.(\d+)]/g, ""),
                          ),
                        });
                      }}
                      keyboardType="numeric"
                    />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Label style={styles.key}> Top Right: </Label>
              </Row>
              <Row>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Latitude</Label>
                    <Input
                      value={
                        this.state.latTopRight
                          ? this.state.latTopRight.toString()
                          : ""
                      }
                      onChangeText={(text) => {
                        this.setState({
                          latTopRight: parseFloat(
                            text.replace(/[^(\d+)\.(\d+)]/g, ""),
                          ),
                        });
                      }}
                      keyboardType="numeric"
                    />
                  </Item>
                </Col>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Longitude</Label>
                    <Input
                      value={
                        this.state.longTopRight
                          ? this.state.longTopRight.toString()
                          : ""
                      }
                      onChangeText={(text) => {
                        this.setState({
                          longTopRight: parseFloat(
                            text.replace(/[^(\d+)\.(\d+)]/g, ""),
                          ),
                        });
                      }}
                      keyboardType="numeric"
                    />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Label style={styles.key}> Bottom Right: </Label>
              </Row>
              <Row>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Latitude</Label>
                    <Input
                      value={
                        this.state.latBottomRight
                          ? this.state.latBottomRight.toString()
                          : ""
                      }
                      onChangeText={(text) => {
                        this.setState({
                          latBottomRight: parseFloat(
                            text.replace(/[^(\d+)\.(\d+)]/g, ""),
                          ),
                        });
                      }}
                      keyboardType="numeric"
                    />
                  </Item>
                </Col>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Longitude</Label>
                    <Input
                      value={
                        this.state.longBottomRight
                          ? this.state.longBottomRight.toString()
                          : ""
                      }
                      onChangeText={(text) => {
                        this.setState({
                          longBottomRight: parseFloat(
                            text.replace(/[^(\d+)\.(\d+)]/g, ""),
                          ),
                        });
                      }}
                      keyboardType="numeric"
                    />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Label style={styles.key}> Bottom Left: </Label>
              </Row>
              <Row>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Latitude</Label>
                    <Input
                      value={
                        this.state.latBottomLeft
                          ? this.state.latBottomLeft.toString()
                          : ""
                      }
                      onChangeText={(text) => {
                        this.setState({
                          latBottomLeft: parseFloat(
                            text.replace(/[^(\d+)\.(\d+)]/g, ""),
                          ),
                        });
                      }}
                      keyboardType="numeric"
                    />
                  </Item>
                </Col>
                <Col>
                  <Item floatingLabel>
                    <Label style={styles.value}>Longitude</Label>
                    <Input
                      value={
                        this.state.longBottomLeft
                          ? this.state.longBottomLeft.toString()
                          : ""
                      }
                      onChangeText={(text) => {
                        this.setState({
                          longBottomLeft: parseFloat(
                            text.replace(/[^(\d+)\.(\d+)]/g, ""),
                          ),
                        });
                      }}
                      keyboardType="numeric"
                    />
                  </Item>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Button
                    onPress={() => {
                      this.onSubmitNewCrop();
                    }}
                    style={{ alignSelf: "center" }}
                    primary
                  >
                    <Text> New SmartFarm </Text>
                  </Button>
                </Col>
                <Col>
                  <Button
                    backgroundColor="#215732"
                    onPress={() => {
                      this.props.navigation.navigate("DrawPolygonMap", {
                        onRegionSelected: ({region})=>this.onRegionSelected({region}),
                      });
                    }}
                    style={{ alignSelf: "center" }}
                    primary
                  >
                    <Text> Mark on Map </Text>
                  </Button>
                </Col>
              </Row>
            </Grid>
          </Form>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  selectedCrop: state.cropsReducer.selectedCrop,
});

export default connect(mapStateToProps)(SmartFarmCreate);
