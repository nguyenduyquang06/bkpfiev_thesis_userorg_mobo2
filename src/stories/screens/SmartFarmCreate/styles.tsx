import { StyleSheet } from "react-native";

const styles: any = StyleSheet.create({
  container: {
    backgroundColor: "#FBFAFA",
  },
  key: {
    fontWeight: "bold",
    fontSize: 17,
    // paddingLeft: 10,
    color: "#215732",
  },
  value: {
    fontSize: 17,
    // paddingLeft: 10,
    color: "#215732",
  },
});
export default styles;
