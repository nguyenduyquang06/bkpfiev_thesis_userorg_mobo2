import * as React from "react";
import {
  Container,
  Header,
  Title,
  Content,
  Body,
  View,
  Picker,
  Icon,
} from "native-base";
import { connect } from "react-redux";
import { MapView } from "expo";
import styles from "./styles";
import { getSensorUpdateHistoryByContractID } from "../../../service/coffeeBatch";
// import { PRODUCT } from "./mock.js";
import _ from "lodash";
import moment from "moment";

export interface Props {
  navigation: any;
}

export interface State {}
class ProductInformation extends React.Component<Props, State> {
  state = {
    selectedContractOrder: 0,
    histData: [],
    mapType: false
  };

  async onValueChange(order) {
    this.setState({
      selectedContractOrder: order
    });
    const param = this.props.navigation.state.params;
    const { contractStepOneID, contractStepTwoID, contractStepThreeID } = param;
    let resp = {};
    switch (order) {
      case 1:
        resp = await getSensorUpdateHistoryByContractID({
          step: order,
          contractID: contractStepOneID
        });
      case 2:
        resp = await getSensorUpdateHistoryByContractID({
          step: order,
          contractID: contractStepOneID
        });
      case 3:
        resp = await getSensorUpdateHistoryByContractID({
          step: order,
          contractID: contractStepOneID
        });
    }
    if (!resp.success) {
      alert("Get history data update failed due to " + resp.message);
      return;
    }
    const histData = JSON.parse(resp.message);
    this.setState({
      histData
    });
  }

  render() {
    const { selectedContractOrder, histData } = this.state;
    return (
      <Container style={styles.container}>
        <Header>
          <Body style={{ flex: 3 }}>
            <Title>Tracking Path</Title>
          </Body>
        </Header>
        <Content>
          <Picker
            mode="dropdown"
            placeholder="Select crop"
            iosIcon={<Icon name="arrow-down" />}
            textStyle={{ color: "#5cb85c" }}
            itemTextStyle={{ color: "#788ad2" }}
            style={{
              width: 150,
              alignSelf: "center",
              alignItems: "flex-end"
            }}
            selectedValue={selectedContractOrder}
            onValueChange={this.onValueChange.bind(this)}
          >
            <Picker.Item label="None" value={0} />
            <Picker.Item label="First Contract" value={1} />
            <Picker.Item label="Second Contract" value={2} />
            <Picker.Item label="Third Contract" value={3} />
          </Picker>
          <Content padder>
            {!_.isEmpty(histData) ? (
              <View>
                <MapView
                  style={{ alignSelf: "stretch", height: 500 }}                  
                  mapType={this.state.mapType ? "satellite" : "standard"}
                  initialRegion={{
                    latitude: _.get(histData[0], ["value", "latitude"]),
                    longitude: _.get(histData[0], ["value", "longitude"]),
                    latitudeDelta: 1,
                    longitudeDelta: 1
                  }}
                >
                  {_.map(histData, o => (
                    <MapView.Marker
                      key={o}
                      coordinate={{
                        longitude: _.get(o, ["value", "longitude"]),
                        latitude: _.get(o, ["value", "latitude"])
                      }}
                      title={moment.unix(o.timestamp).format("MM-DD-YYYY hh:mm:ss")}
                      description={`${o.value.temperature}*C, ${o.value.humidity}%`}
                    />
                  ))}
                </MapView>
              </View>
            ) : null}
          </Content>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  bkcBalance: state.userReducer.bkcBalance
});

export default connect(mapStateToProps)(ProductInformation);
